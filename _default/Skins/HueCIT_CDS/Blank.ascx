<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<!--#include file="layouts/default/_includes-top.ascx" -->




<div class="group-content-areas">
    <section class="container-module">
            <div class="row"><div class="col-md-12"><div id="ModulePane" runat="server" /></div></div>
    </section>

    <div id="content-areas">

        <div class="container">
            <div class="row"><div class="col-md-12"><div id="ContentPane" runat="server" /></div></div>
        </div><!-- End : Content Pane -->

    </div> <!-- ./content-areas -->
</div>


<!--#include file="layouts/default/search-modal.ascx" -->

<!-- Go To Top Link -->
<div class="back-to-top">
    <a href="#"></a>
</div>

<!--#include file="layouts/default/_includes-bottom.ascx" -->

<script type="text/javascript">
    $('.btn-sidebar').click(function(){
      $("#sidebar").toggleClass("show")
    })

    $('.btn-sidebar-close').click(function(){
      $("#sidebar").toggleClass("show")
    })
</script>


<script>

    function resize() {

        var imgListcategoryhWidth = $('.DsChuyenMucTinBai .news_list_chuyenmuc ul.listcategory li .listcategory__img').width();
        var heightImgListcategory = $('.DsChuyenMucTinBai .news_list_chuyenmuc ul.listcategory li .listcategory__img');
        var heightsImgListcategory = imgListcategoryhWidth/1.54368932039;
        for(var i=0;i<heightImgListcategory.length;i++){
            heightImgListcategory[i].style.height = heightsImgListcategory + "px";
        }

        var imgListcategoryhWidth1 = $('.ModHueTourisEventViewOrtherC .other_events .Orther-img ').width();
        var heightImgListcategory1 = $('.ModHueTourisEventViewOrtherC .other_events .Orther-img ');
        var heightsImgListcategory1 = imgListcategoryhWidth1/1.53883495146;
        for(var i=0;i<heightImgListcategory1.length;i++){
            heightImgListcategory1[i].style.height = heightsImgListcategory1 + "px";
        }


    }
    $(document).ready(function () {
        window.addEventListener("resize", resize);
        window.onresize = function () {
            resize();
        };
            resize();
    });


    var
        Titlemain = $('.news_list_chuyenmuc > .container_module_title > .circle > a').text();
        Titlemain2 = $('.news_list_detail > .container_module_title > .circle > a').text();
        lengths = $('.container-content .breadcrumb .container > span > span > span').length;
        
        if(lengths >=4){
            Titlemain3 = $('.container-content .breadcrumb .container > span > span > span:nth-of-type(3) span').text();
        } else{
            Titlemain3 = $('.container-content .breadcrumb .container > span > span > span:last-child a').text();
        }
        
    $('#MainMenu > .accordion > .card').each(function(){
        if ($(this).children('a').text() == Titlemain) {
            $(this).addClass('active');
        }

        if ($(this).children('a').text() == Titlemain2) {
            $(this).addClass('active');
        }
        if ($(this).children('a').text() == Titlemain3) {
            $(this).removeClass('active');
            $(this).addClass('active');
        }
    });

    var lengths1 = $('.container-content .breadcrumb .container > span > span > span > span').text();
        console.log(lengths1)
    
        
    $('#main-menu > li > a').each(function(){
        if ($(this).text() == lengths1) {
            $(this).removeClass('current');
            $(this).addClass('current');
        }
    });

    $(document).ready(function(){
    $.fn.accordion = function() {
      const trigger = $(this).find('.accordion-trigger');
      const collapse = $(this).find('.accordion-collapse');

      $(trigger).first().addClass('accordion-open');
      $(collapse).first().show();

      $(trigger).each(function(){
        $(this).on('click', function(){
          $(this).siblings('.accordion-collapse').slideToggle();
          $(this).toggleClass('accordion-open');
          $(this).parent().siblings('.accordion-item').find('.accordion-trigger').removeClass('accordion-open');
          $(this).parent().siblings('.accordion-item').find('.accordion-collapse').slideUp();
        });
      });
    }

    $('.accordion').accordion();
});


    // if(lengths >=4){
    //     $( "#MainMenu #accordion > .card .collapse a" ).each(function() {
    //         if($(this).text() == $('.container-content .breadcrumb .container > span > span > span:last-child a').text()) {
    //             $(this).addClass('active');
    //         }
    //     });
    // } 

    // var lengths = $('.breakcrum a').length;
    //     if(lengths >=3){
    //         var Titlemain = $('.breakcrum a:nth-of-type(3)').text();
        
            
    //     } else{
    //         var Titlemain = $('.breakcrum a:last-child').text();
    //     }
    //     $('#main-menu > li > a').each(function(){
    //         if ($(this).text() == Titlemain) {
    //             $(this).removeClass('current');
    //             $(this).addClass('current');    
    //         }

    //     });

    /* var ac = $('.module_titleh4').text();
        $('#MainMenu > .accordion > .card').each(function(){
            if ($(this).children('a').text() == ac) {
                $(this).removeClass('active');
                $(this).addClass('active');
            }
        }); */

</script>
