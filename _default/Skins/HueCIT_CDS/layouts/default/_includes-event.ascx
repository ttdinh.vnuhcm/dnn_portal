﻿
<dnn:META ID="mobileScale" runat="server" Name="viewport" Content="width=device-width, initial-scale=1.0" />
<dnn:JQUERY ID="dnnjQuery" runat="server" jQueryHoverIntent="true" />
<dnn:DnnCssInclude ID="DnnCssThemify" runat="server" FilePath="assets/css/themify-icons.css" PathNameAlias="SkinPath" Name="Themify" Version="1.0.0" />
<dnn:DnnCssInclude ID="DnnCssSmartMenus" runat="server" FilePath="assets/css/smartmenus.css" PathNameAlias="SkinPath" Name="smartmenus" Version="1.0.1" />
<dnn:DnnCssInclude ID="DnnCssOwlcarousel" runat="server" FilePath="assets/css/owl.carousel.min.css" PathNameAlias="SkinPath" Name="Owlcarousel" Version="2.3.4" />
<dnn:DnnCssInclude ID="DnnCssOwltheme" runat="server" FilePath="assets/css/owl.theme.default.min.css" PathNameAlias="SkinPath" Name="Owltheme" Version="2.3.4" />
<dnn:DnnCssInclude ID="DnnCssAnimate" runat="server" FilePath="assets/css/animate.min.css" PathNameAlias="SkinPath" Name="animatecss" Version="3.5.2" />
