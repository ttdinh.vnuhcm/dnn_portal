﻿<div class="copyright-section">
    <div class="row">
        <div class="col-12">
            <div class="footerbottom_right">
                <%-- <dnn:COPYRIGHT ID="dnnCopyright" runat="server" />--%>
                <div class="subbackground-container">
                    <div class="sub-footer">
                        <p class="Copyright">2022 Bản quyền thuộc về Sở thông tin và truyền thông tỉnh Thừa Thiên Huế</p>
                        <div class="built-in">
                            <div class="tooltip_constrast">
                                <img alt="" style="width: 24px; height: 24px;" src="/Portals/_default/Skins/HueCIT_CDS/assets/images/constrast.svg">
                                <!-- <input class="tooltiptext" id="myRange" min="0" max="200" onchange="showVal(this.value)" type="range"> -->
                            </div>
                            <p class="Built">Thiết kế và xây dựng: <span class="title-bold">HueCIT</span></p>
                        </div>
                    </div>
            </div>
                <!-- <div class="text-copyright">
                    Xây dựng và thiết kế bởi <a href="http://www.huecit.vn/" target="_blank">HueCIT</a>
                </div>
                <div class="tooltip_constrast">
                    <img alt="" style="width: 24px; height: 24px;" src="/Portals/_default/Skins/HueCIT_KPH/assets/images/hotrokhuyettat.svg">
                    <input class="tooltiptext" id="myRange" min="0" max="200" onchange="showVal(this.value)" type="range">
                </div>
                <div class="img-tinnhiem">
                    <a href="https://tinnhiemmang.vn/danh-ba-tin-nhiem/khamphahuecomvn-1643332083" title="Chung nhan Tin Nhiem Mang" target="_blank">
                        <img src="https://tinnhiemmang.vn/handle_cert?id=khamphahue.com.vn" alt="Chung nhan Tin Nhiem Mang" width="150px" height="auto">
                    </a>
                </div>
                <div class="icon-linked">
                    <div class="icon-facebook">
                        <a href="https://www.facebook.com/khamphahue.com.vn/" target="_blank">
                            <img alt="" src="/Portals/_default/skins/huecit_kph/assets/images/icon-facebook.png"> 
                        </a>
                    </div>
                    <div class="icon-instagram">
                        <a href="https://www.instagram.com/khamphahue/" target="_blank">
                            <img alt="" src="/Portals/_default/skins/huecit_kph/assets/images//icon-instagram.png"> 
                        </a>
                    </div>
                    <div class="icon-youtube">
                        <a href="https://www.youtube.com/channel/UCbpRQS3eALTNBd5_gYuraHw" target="_blank">
                            <img alt="" src="/Portals/_default/skins/huecit_kph/assets/images/icon-youtube.png"> 
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </div><!-- ./row -->
</div>
<script type="text/javascript">
    function showVal(newVal) {
            document.getElementsByTagName("body")[0].setAttribute('style', 'color:blue;filter: brightness(' + newVal + '%);');
        }
</script>