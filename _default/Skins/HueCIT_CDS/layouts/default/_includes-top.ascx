﻿
<dnn:META ID="mobileScale" runat="server" Name="viewport" Content="width=device-width, initial-scale=1.0" />
<dnn:JQUERY ID="dnnjQuery" runat="server" jQueryHoverIntent="true" />
<dnn:DnnCssInclude ID="DnnCssBootStrap" runat="server" FilePath="assets/css/bootstrap.css" PathNameAlias="SkinPath" Name="bootstrap" Version="4.0.1-alpha.6" />
<dnn:DnnCssInclude ID="DnnCssThemify" runat="server" FilePath="assets/css/themify-icons.css" PathNameAlias="SkinPath" Name="Themify" Version="1.0.0" />
<dnn:DnnCssInclude ID="DnnCssMaterial" runat="server" FilePath="assets/css/material-icons-full.css" PathNameAlias="SkinPath" Name="Material" Version="1.0.0" />
<dnn:DnnCssInclude ID="DnnCssSmartMenus" runat="server" FilePath="assets/css/smartmenus.css" PathNameAlias="SkinPath" Name="smartmenus" Version="1.0.1" />
<dnn:DnnCssInclude ID="DnnCssOwlcarousel" runat="server" FilePath="assets/css/owl.carousel.min.css" PathNameAlias="SkinPath" Name="Owlcarousel" Version="2.3.4" />
<dnn:DnnCssInclude ID="DnnCssOwltheme" runat="server" FilePath="assets/css/owl.theme.default.min.css" PathNameAlias="SkinPath" Name="Owltheme" Version="2.3.4" />
<dnn:DnnCssInclude ID="DnnCssAnimate" runat="server" FilePath="assets/css/animate.min.css" PathNameAlias="SkinPath" Name="animatecss" Version="3.5.2" />
<%--<dnn:DnnCssInclude ID="DnnCssStyle" runat="server" FilePath="assets/css/dnnstyle.css?v=0.0.2" PathNameAlias="SkinPath" Name="dnnstyle" Version="0.0.2" />--%>



<!-- Theme pre-defined color scheme -->
<dnn:DnnCssInclude ID="DnnCssColorScheme" runat="server" FilePath="assets/css/colors/_base.css?v=0.0.2" PathNameAlias="SkinPath" Name="colorscheme" Version="0.0.2" />
<!-- Theme pre-defined color scheme -->

<dnn:DnnCssInclude ID="DnnCssInclude11" runat="server" FilePath="assets/css/long.css?v=0.0.2" PathNameAlias="SkinPath" Name="long" Version="0.0.3" />

<dnn:DnnCssInclude ID="DnnCssInclude12" runat="server" FilePath="assets/css/quy.css?v=0.0.2" PathNameAlias="SkinPath" Name="quy" Version="0.0.2" />

<%--<dnn:DnnCssInclude ID="DnnCssResponsive" runat="server" FilePath="assets/css/responsive.css?v=0.0.2" PathNameAlias="SkinPath" Name="dnnResponsive" Version="0.0.2" />--%>
