﻿<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<script src="/Portals/_default/skins/huecit_cds/assets/js/owl.carousel.min.js?cdv=51" type="text/javascript"></script>

<!--#include file="layouts/default/_includes-top.ascx" -->

<!-- Start Header Section -->

<header class="header home-layout">
    <div class="menu-bar">
        <div class="overlay-blur"></div>
        <div class="container">
            <!--#include file="layouts/default/nav-bar.ascx" -->
        </div>
    </div>
    <!-- ./navbar -->
</header><!-- ./Header -->

<!-- Start Slider area -->
<div class="slide-areas">
    <div id="sliderPane" runat="server"></div>
    <div class="container sukien">
        <div id="NotificationPane1" runat="server"></div>
    </div>
    <!--#include file="layouts/default/search-modal.ascx" -->
 <!--    <div class="home-search">
        <div class="search-group">
            <dnn:SEARCH ID="dnnSearch1" runat="server" 
                        ShowSite="false" 
                        ShowWeb="false" 
                        EnableTheming="true" 
                        CssClass="SearchButton" />
            <div class="icon-search">
                <img src="/Portals/_default/Skins/HueCIT_KPH/assets/images/icon-search-home.png" alt="">
            </div>
        </div>
    </div> -->

</div>
<!-- ./Slider area -->
<section class="section-Tintucsk">
    <div class="container">
        <div class="row">
            <div id="TintucskPane_Full" class="section-title col-12" runat="server"></div>
         
        </div>
    </div>
</section>
<section class="section-info-digital">
    <div class="container">
        <div class="row">
            <div id="info_Many" class="col-12" runat="server"></div>
        </div>
    </div>
</section>
<section class="notification bg-body-2">
    <div class="container">
        <div id="NotificationPane" runat="server"></div>
    </div>
</section>
<section class="bg-body-2 topic">
    <div class="container cm-topic">
        <div class="specializedpane">
            <div id="specializedPane_1" class="col-pane" runat="server"></div>
            <div id="specializedPane_2" class="col-pane" runat="server"></div>
            <div id="specializedPane_3" class="">
                <div class="row">
                    <div id="specializedPane_3A" class="col-sm-4 Leftpane-TLHDSD" runat="server"></div>
                    <div id="specializedPane_3B" class="col-sm-8 Rightpane-TLHDSD" runat="server"></div>
                </div>

            </div>
            <div id="specializedPane_4" class="col-pane" runat="server"></div>
        </div>
        <!--Icon background-->
       <!--  <div class="icon-bg" id="icon-bg-01">
            <img src="Portals/_default/Skins/HueCIT_KPH/assets/images/icon-bg-01.png" alt="">
        </div>
        <div class="icon-bg" id="icon-bg-02">
            <img src="Portals/_default/Skins/HueCIT_KPH/assets/images/icon-bg-02.png" alt="">
        </div>
        <div class="icon-bg" id="icon-bg-03">
            <img src="Portals/_default/Skins/HueCIT_KPH/assets/images/icon-bg-03.svg" alt="">
        </div> -->
    </div>
</section>
<section class="section-HUE-S">
    <div class="container">
        <div class="row">
    
            <div id="HUESPane_1" class="col-md-5 col-Hues1" runat="server"></div>
            <div id="HUESPane_2" class="col-md-7 col-Hues2" runat="server"></div>
        </div>
    </div>
</section>

<section class="section-event">
    <div class="container">
        <div class="row">
            <div id="EventPane" class="col-12" runat="server"></div>
        </div>
    </div>
</section>


<section class="section-spchuyendoiso">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div id="spchuyendoisoPane_2" class="enterprise-hot noTitleCM slide-hot" runat="server"></div>
            </div>
        </div>
    </div>
</section>





<section class="section-thuvienso">
    <div class="container">
        <div class="row">
            <div id="ThuviensoPane_Full" class="section-title col-12" runat="server"></div>
        </div>
    </div>
    <div id="ThuviensoPane_1" class="section-title" runat="server"></div>
</section>

<%-- <section class="section-ads">
    <div class="container">
        <div class="row">
            <div id="Ads" class="col-12" runat="server"></div>
        </div>
    </div>
</section> --%>

<div id="content-areas">

    <div class="container">
        <div class="row"><div class="col-md-12"><div id="ContentPane" runat="server" /></div></div>
    </div><!-- End : Content Pane -->

</div> <!-- ./content-areas -->

<!-- Start Footer -->
<footer>
    <div class="footer-top">
        <div class="container">
            <!--#include file="layouts/default/footer-main.ascx" -->
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <!--#include file="layouts/default/footer-legal.ascx" -->
        </div>
    </div>
</footer><!-- ./Footer -->


<!-- Go To Top Link -->
<div class="back-to-top">
    <a href="#"></a>
</div>



<!--#include file="layouts/default/_includes-bottom.ascx" -->

<script type="text/javascript">
     $('.slide-areas .owl-theme').owlCarousel({
    loop:true,
    margin:24,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    nav:false,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        },
        1024:{
            items:1
        },
        1080:{
            items:1
        }
    }
})
$('#dnn_HUESPane_1 .owl-theme').owlCarousel({
    items:1,
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    nav:false,
    dots:true,
})
    $('#dnn_TintucskPane_Full .owl-theme').owlCarousel({
    loop:true,
    margin:24,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    nav:false,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        },
        1024:{
            items:3
        },
        1080:{
            items:4
        }
    }
})
/* $('#dnn_specializedPane_2 .owl-theme').owlCarousel({
    loop:true,
    margin:10,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    nav:true,
    dots:false,
    navText: ["<img src='/Portals/_default/Skins/HueCIT_CDS/assets/images/btn-Previous.png'>", "<img src='/Portals/_default/Skins/HueCIT_CDS/assets/images/btn-Next.png'>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        900:{
            items:2
        },
        1024:{
            items:3
        },
        1080:{
            items:3
        }
    }
}) */
$('#dnn_specializedPane_3B .owl-theme').owlCarousel({
    loop:true,
    //margin:24,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    nav:true,
    dots:false,
    navText: ["<img src='/Portals/_default/Skins/HueCIT_CDS/assets/images/btn-Previous.png'>", "<img src='/Portals/_default/Skins/HueCIT_CDS/assets/images/btn-Next.png'>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        750:{
            items:2
        },
        900:{
            items:2
        },
        1024:{
            items:2
        },
        1080:{
            items:2
        }
    }
})
$('#dnn_specializedPane_4 .owl-theme').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    nav:true,
    dots:false,
    navText: ["<img src='/Portals/_default/Skins/HueCIT_CDS/assets/images/btn-Previous.png'>", "<img src='/Portals/_default/Skins/HueCIT_CDS/assets/images/btn-Next.png'>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        },
        1024:{
            items:3
        },
        1080:{
            items:3
        }
    }
})  
$('#dnn_spchuyendoisoPane_2 .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    nav:true,
    dots:false,
    navText: ["<img src='/Portals/_default/Skins/HueCIT_CDS/assets/images/btn-Previous.png'>", "<img src='/Portals/_default/Skins/HueCIT_CDS/assets/images/btn-Next.png'>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        },
        1024:{
            items:1
        },
        1080:{
            items:1
        }
    }
})   

function resize() {

        var widthImg = $('#dnn_specializedPane_3B .item .slide-bg img').width();
        var heightImg = $('#dnn_specializedPane_3B .item .slide-bg img');
        var heightsImg = widthImg/1.33064516129;
        for(var i=0;i<heightImg.length;i++){
            heightImg[i].style.height = heightsImg + "px";
        }

        var widthImg1 = $('.section-spchuyendoiso .item-img img').width();
        var heightImg1 = $('.section-spchuyendoiso .item-img img');
        var heightsImg1 = widthImg1/1.73684210526;
        for(var i=0;i<heightImg1.length;i++){
            heightImg1[i].style.height = heightsImg1 + "px";
        }

        var widthImg2 = $('#dnn_TintucskPane_Full .item .slide-image img').width();
        var heightImg2 = $('#dnn_TintucskPane_Full .item .slide-image img');
        var heightsImg2 = widthImg2/1.54368932039;
        for(var i=0;i<heightImg2.length;i++){
            heightImg2[i].style.height = heightsImg2 + "px";
        }

        var widthImg3 = $('.slide-areas .backgroud-HueS img').width();
        var heightImg3 = $('.slide-areas .backgroud-HueS img');
        var heightsImg3 = widthImg3/3.49090909091;
        for(var i=0;i<heightImg3.length;i++){
            heightImg3[i].style.height = heightsImg3 + "px";
        }

        var widthTLHDSD = $('.menu-m20 .owl-stage-outer .item').width();
        var heightTLHDSD = $('.menu-m20 .owl-stage-outer .item');
        var height5 = $(".Leftpane-TLHDSD .TLHD-SD");
        var heightsTLHDSD = widthTLHDSD/1.1350802139;
        for(var i=0;i<heightTLHDSD.length;i++){
            heightTLHDSD[i].style.height = heightsTLHDSD + "px";
        }
        height5.innerHeight(heightsTLHDSD);
        

    }

$(document).ready(function () {
    window.addEventListener("resize", resize);
    window.onresize = function () {
        resize();
    };
        resize();
});



$(document).ready(function(){

function heightBg(){
    var widthImg = $("#dnn_HUESPane_1 .bg-white-hues ").width();
    var heightBg = $("#dnn_HUESPane_1 .bg-white-hues ");
    heightBg.height(widthImg/0.53594771241);


};

setTimeout(function() { 
    heightBg();
}, 1000);

/* window.addEventListener("height", height);
window.onresize = function () {
    height();

};
    height();*/
});


// Lazy Load Images using Intersection Observer
(function () {
        var observer = new IntersectionObserver(onIntersect);

        document.querySelectorAll("[data-lazy]").forEach((img) => {
            observer.observe(img);
        });

        function onIntersect(entries) {
            entries.forEach((entry) => {
                if (entry.target.getAttribute("data-processed") || !entry.isIntersecting)
                    return true;
                entry.target.setAttribute("src", entry.target.getAttribute("data-src"));
                entry.target.setAttribute("data-processed", true);
            });
        }
    })();
</script>




    