<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<!--#include file="layouts/default/_includes-top.ascx" -->

<!-- Start Header Section -->

<header class="header content-layout">
    <div class="menu-bar">
        <div class="overlay-blur"></div>
        <div class="container">
            <!--#include file="layouts/default/nav-bar.ascx" -->
        </div>
    </div>
    <!-- ./navbar -->
</header><!-- ./Header -->

<section class="container-content">
    <div class="breadcrumb">
        <div class="container">
            <div id="breadcrumb_menu" runat="server"></div>
        </div>
    </div>
    <div class="container-content">
        <div class="container">
            <div class="row">
                <main id="sidebar" class="col-lg-3 col-md-4">
                    <a title="Close sidebar" role="button" class="btn-sidebar-close"><i class="material-icons-outlined">close</i></a>
                    <div class="row row-offcanvas row-offcanvas-left HueCIT_NoTitle_Content_Detail">
                        <div id="Row1_LeftPane" runat="server" class="col-md-12 hotnews noCM group-3"></div>
                        <div id="Row2_LeftPane" runat="server" class="col-md-12 hotnews group-3"></div>
                        <div id="Row3_LeftPane" runat="server" class="col-md-12 hotnews group-3"></div>                        
                        <div class="col-md-12 lit-art">
                            <div class="lit-art topic-menuList" id="lit_Art" runat="server"></div>
                            <div id="Row4_LeftPane" runat="server" class="hotnews group-3"></div>
                        </div>
                    </div>

                </main>
                <div id="GridLeft" class="col-lg-9 col-md-8">
                    <a title="Sidebar right" class="btn-sidebar" role="button"><span class="material-icons-outlined">menu_open</span></a>
                    <div class="row">
                        <div id="Row_RightPane" runat="server" class="col-md-12 most_read"></div>
                        <div id="Row_RightPane2" runat="server" class="col-md-12 most_read2"></div>
                        <div id="Row_RightPane_Many_CM" runat="server" class="col-md-12 most_read noNewsLink"></div>
                        <div id="Row_RightPane_Accordion_NoIco" runat="server" class="col-md-12 most_read noNewsLink"></div>
                        <div id="Row_RightPane_Accordion_Ico" runat="server" class="col-md-12 most_read noNewsLink"></div>
                    </div>
                </div>
                <div id="RowFull_LeftPane" runat="server" class="col-md-12 hotnews group-3"></div>
            </div>
        </div>
    </div>
</section>

<div id="content-areas">

    <div class="container">
        <div class="row"><div class="col-md-12"><div id="ContentPane" runat="server" /></div></div>
    </div><!-- End : Content Pane -->

</div> <!-- ./content-areas -->

<!-- Start Footer -->
<footer>
    <div class="footer-top">
        <div class="container">
            <!--#include file="layouts/default/footer-main.ascx" -->
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <!--#include file="layouts/default/footer-legal.ascx" -->
        </div>
    </div>
</footer><!-- ./Footer -->

<!--#include file="layouts/default/search-modal.ascx" -->

<!-- Go To Top Link -->
<div class="back-to-top">
    <a href="#"></a>
</div>

<!--#include file="layouts/default/_includes-bottom.ascx" -->

<script type="text/javascript">
    $('.btn-sidebar').click(function(){
      $("#sidebar").toggleClass("show")
    })

    $('.btn-sidebar-close').click(function(){
      $("#sidebar").toggleClass("show")
    })



    var
        Titlemain = $('.news_list_chuyenmuc > .container_module_title > .circle > a').text();
        Titlemain2 = $('.news_list_detail > .container_module_title > .circle > a').text();
        lengths = $('.container-content .breadcrumb .container > span > span > span').length;
        if(lengths >=4){
            Titlemain3 = $('.container-content .breadcrumb .container > span > span > span:nth-of-type(3) span').text();
        } else{
            Titlemain3 = $('.container-content .breadcrumb .container > span > span > span:last-child a').text();
           
        }
      var  Titlemain4 = $('.container-content .breadcrumb .container > span > span > span:last-child a').text();
      $("#MainMenu > .accordion > .card > .collapse >a").each(function() {
        if($(this).text()==Titlemain4){
            $(this).addClass('active');
        }
      })
    
    $('#MainMenu > .accordion > .card').each(function(){
        if ($(this).children('a').text() == Titlemain) {
            $(this).addClass('active');
        }

        if ($(this).children('a').text() == Titlemain2) {
            $(this).addClass('active');
        }
        if ($(this).children('a').text() == Titlemain3) {
            $(this).removeClass('active');
            $(this).addClass('active');
        }
    });

   
    var lengths1 = $('.container-content .breadcrumb .container > span > span > span > span').text();
    
        
    $('#main-menu > li > a').each(function(){
        if ($(this).text() == lengths1) {
            $(this).removeClass('current');
            $(this).addClass('current');
        }
    });

</script>

