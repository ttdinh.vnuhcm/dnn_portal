<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<!--#include file="layouts/default/_includes-top.ascx" -->
<style>
#fof{display:block; position:relative; width:100%; height:529px; background:url("Portals/_default/Skins/HueCIT_CDS/assets/images/404.png") top center no-repeat; line-height:1.6em; text-align:center;}
#fof .positioned{padding-top:80px;}
#fof .hgroup{margin-bottom:10px; text-transform:uppercase;}
#fof .hgroup h1{margin-bottom:0; font-size:160px; line-height:120px;color: #919191;margin-top: 0;}
#fof .hgroup h2{margin-bottom:0; font-size:80px; color: #919191;margin-top: 0; line-height:1}
#fof p{display:block; margin:0 0 25px 0; padding:0; font-size:16px; color: #919191}
#container {
    padding: 30px 0;
}
div.wrapper {
    display: block;
    margin: 0;
    padding: 0;
    text-align: left;
    width: 100%;
}
.row2 {
    background-color: #ffffff;
    color: #979797;
}
#container a {
    background-color: transparent;
}
.row2 a {
    background-color: #ffffff;
    color: #00B2FF;
}
</style>
<!-- Start Header Section -->

<header class="header content-layout contact-page">
    <div class="menu-bar">
        <div class="overlay-blur"></div>
        <div class="container">
            <!--#include file="layouts/default/nav-bar.ascx" -->
        </div>
    </div>
    <!-- ./navbar -->
</header><!-- ./Header -->

<section>
    <div id="ContentPane" runat="server"></div>    
</section>


<!-- Start Footer -->
<footer>
    <div class="footer-top">
        <div class="container">
            <!--#include file="layouts/default/footer-main.ascx" -->
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <!--#include file="layouts/default/footer-legal.ascx" -->
        </div>
    </div>
</footer><!-- ./Footer -->

<!--#include file="layouts/default/search-modal.ascx" -->



<!--#include file="layouts/default/_includes-bottom.ascx" -->
