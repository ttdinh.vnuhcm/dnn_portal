<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<!--#include file="layouts/default/_includes-top.ascx" -->

<!-- Start Header Section -->
<header class="header Media_header">
    <div class="menu-bar mini-menubar">
        <div class="overlay-blur"></div>
        <div class="container">
            <nav class="contra-nav" role="navigation">
                <input id="main-menu-state" type="checkbox" />
                <label class="main-menu-btn" for="main-menu-state">
                    <span class="main-menu-btn-icon"></span> Toggle main menu visibility
                </label>
                <div class="full-mini-navbar">
                    <div class="mini-navlogo">
                        <div class="nav-brand"><dnn:LOGO runat="server" ID="dnnLOGO" /></div>
                    </div>
                    <div class="small-navbar">
                        <div class="row">
                            <div class="col-12 mini-navbar rightmininav" runat="server" id="Navbar_RightPane"></div>
                        </div>
                    </div> 
                </div>
                </div>
            </nav>
        </div>
    </div>
    <!-- ./navbar -->
</header><!-- ./Header -->

<section class="container-content havedimbackground">
    <section id="Trang-chu" class="onepage">
        <div id="MainPurpleBanner_Introduce" runat="server" class="banner Imgintroduce Purpleborder"></div>
        <div id="MainYellowBanner_Introduce" runat="server" class="banner Imgintroduce Yellowborder"></div>
    </section>
    <div class="hiddenbackgroundimage">
        <div id="HiddenPicMain_Pane" runat="server" class="blurpicture"></div>
    </div>
    <div class="container-content">
        <section id="Gioi-thieu" class="section-Introduce Purplebackground_Intro onepage">
            <div class="hidden-intropic">
                <div id="HiddenPic1_Pane" runat="server" class="blurpicture"></div>
            </div>
            <div class="container">
                <div class="row main-introduce purplebackground">
                    <div id="MainPurpleIntroduce_Pane" runat="server" class="col-12 tabcontent_news introduce"></div>
                </div>
            </div>
        </section>
        <section id="Dien-gia" class="section-specialties section-col-4-pane noTitleDes DienGia onepage">
            <div class="container">
                <div class="row info-des">
                        <div class="Specialties col-12">
                            <div id="Specialties_Contents" runat="server" class="hotnews specialties_content nodate haveheadtitle"></div>
                        </div>
                </div>
            </div>
        </section>
        <section id="Chuong-trinh" class="section-specialties section-col-4-pane noTitleDes ChuongTrinh onepage">
            <div class="container">
                <div class="row info-des">
                        <div class="Specialties col-12">
                            <div id="Specialties_Shops" runat="server" class="hotnews specialties_content nodate haveheadtitle"></div>
                        </div>
                </div>
            </div>
        </section>
        <section id="Tai-tro" class="section-content-full TaiTro onepage">
            <div class="container">
                <div class="row">
                    <div id="Specializing_NewsContent" runat="server" class="col-12 hotnews noCM container-nocm content-full Specializing_content specialties_content nodate background-full"></div>
                    <div id="SpecializingNoBG_NewsContent" runat="server" class="col-12 hotnews noCM container-nocm content-full Specializing_content specialties_content nodate Nobackground haveheadtitle"></div>
                </div>
            </div>
        </section>
        <section id="Lien-he" class="section-content-full LienHe onepage">
            <div class="container">
                <div class="row">
                    <div id="SpecializingNoBG_HueContent" runat="server" class="col-12 hotnews noCM container-nocm content-full Specializing_content specialties_content Nobackground haveheadtitle"></div>
                </div>
            </div>
        </section>
        <section id="Cong-ty" class="section-content-full Doanhnghiep onepage">
            <div class="container">
                <div class="row">
                    <div id="SpecializingNoBG_Company" runat="server" class="col-12 hotnews noCM container-nocm content-full Specializing_content specialties_content Nobackground haveheadtitle"></div>
                </div>
            </div>
        </section>
        <section id="Huong-nghiep" class="section-guide Huongnghiep onepage">       
            <div class="container">
                <div class="row info-des">
                    <div id="GuideNoBG_NewsContent" runat="server" class="col-12 hotnews content-full Guide_content specialties_content Nobackground nodate haveheadtitle"></div>
                </div>
            </div>
        </section>
        <section id="Tuyen-sinh" class="section-recuiting section-col-4-pane Tuyensinh onepage">       
            <div class="container">
                <div class="row info-des">
                        <div class="Recuit col-12">
                            <div id="Recuit_Contents" runat="server" class="hotnews recuit_content specialties_content nodate haveheadtitle"></div>
                        </div>
                </div>
            </div>
        </section>
        <section id="Thu-vien" class="section-library Thuvien onepage">
            <div class="container">
                <div class="row">
                        <div class="Specialties-MediaLibrary">
                            <div class="Library-PurpleBack">
                                <div id="Specialties_TitleLibrary" runat="server" class="col-12 media_title"></div>
                                <div id="Specialties_VideoLibrary" runat="server" class="col-md-6 media_content notitle P_Back"></div>
                                <div id="Specialties_ImageLibrary" runat="server" class="col-md-6 media_content notitle P_Back"></div>
                            </div>
                            <div class="Library-YellowBack">
                                <div id="Specialties_TitleLibrary_Y" runat="server" class="col-12 media_title"></div>
                                <div id="Specialties_VideoLibrary_Y" runat="server" class="col-md-6 media_content notitle Y_Back"></div>
                                <div id="Specialties_ImageLibrary_Y" runat="server" class="col-md-6 media_content notitle Y_Back"></div>
                            </div>
                        </div>
                </div>
            </div>
        </section>
        <section id="Hinh-anh" class="section-recuitmedia Hinhanh onepage">       
            <div class="container">
                <div class="row">
                        <div class="RecuitMeida col-12">
                            <div id="RecuitMedia_Contents" runat="server" class="hotnews recuitmedia_content specialties_content nodate "></div>
                        </div>
                </div>
            </div>
        </section>
    </div>
</section>

<div id="content-areas">

    <div class="container">
        <div class="row"><div class="col-md-12"><div id="ContentPane" runat="server" /></div></div>
    </div><!-- End : Content Pane -->

</div> <!-- ./content-areas -->

<!-- Start Footer -->
<footer>
    <div class="footer-top">
        <div class="container">
            <!--#include file="layouts/default/footer-main.ascx" -->
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <!--#include file="layouts/default/footer-legal.ascx" -->
        </div>
    </div>
</footer><!-- ./Footer -->

<!--#include file="layouts/default/search-modal.ascx" -->

<!-- Go To Top Link -->
<div class="back-to-top">
    <a href="#"></a>
</div>

<!--#include file="layouts/default/_includes-bottom.ascx" -->

<script type="text/javascript">
    $('.btn-sidebar').click(function(){
      $("#sidebar").toggleClass("show")
    })

    $('.btn-sidebar-close').click(function(){
      $("#sidebar").toggleClass("show")
    })

    
</script>

<script>
    // SmartMenus init
    $(function () {
        $('.small-navbar .menu-m9 ul').smartmenus({
            subMenusSubOffsetX: 1,
            subMenusSubOffsetY: -8
        });
    });

    // SmartMenus mobile menu toggle button
    $(function () {
        var $mainMenuState = $('#main-menu-state');
        if ($mainMenuState.length) {
            // animate mobile menu
            $mainMenuState.change(function (e) {
                var $menu = $('.small-navbar .menu-m9 ul');
                if (this.checked) {
                    $menu.hide().slideDown(250, function () { $menu.css('display', ''); });
                } else {
                    $menu.show().slideUp(250, function () { $menu.css('display', ''); });
                }
            });
            // hide mobile menu beforeunload
            $(window).bind('beforeunload unload', function () {
                if ($mainMenuState[0].checked) {
                    $mainMenuState[0].click();
                }
            });
        }
    });
</script>

<script>
function resizeDigitalHue24h() {
        var imgDigitalHue24hWidth = $('.module_container_news3img .img_left_large .group_scoll .img_scrollnews').width();
        var heightImgDigitalHue24h = $(".module_container_news3img .img_left_large .group_scoll .img_scrollnews");
        var heightsImgDigitalHue24h = imgDigitalHue24hWidth/1.91666666667;
        for(var i=0;i<heightImgDigitalHue24h.length;i++){
            heightImgDigitalHue24h[i].style.height = heightsImgDigitalHue24h + "px";
        }
    }

    function resizeRightDigitalHue24h() {
        var imgRightDigitalHue24hWidth = $('.module_container_news3img .img_right_list .group_scoll .img_scrollnews').width();
        var heightImgRightDigitalHue24h = $(".module_container_news3img .img_right_list .group_scoll .img_scrollnews");
        var heightsImgRightDigitalHue24h = imgRightDigitalHue24hWidth/1.62142857143;
        for(var i=0;i<heightImgRightDigitalHue24h.length;i++){
            heightImgRightDigitalHue24h[i].style.height = heightsImgRightDigitalHue24h + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resizeDigitalHue24h", resizeDigitalHue24h);
        window.addEventListener("resizeRightDigitalHue24h", resizeRightDigitalHue24h);
        window.onresize = function () {
            resizeDigitalHue24h();
            resizeRightDigitalHue24h();
        };
            resizeDigitalHue24h();
            resizeRightDigitalHue24h();
    });
</script>

<script>
    /******************************************************************/
    function resizeHotNews() {
        var imgHotNewshWidth = $('.hotnews .dstinbailietkeanh_tomtat .news_list ul li .hot_news').width();
        var heightImgHotNews = $(".hotnews .dstinbailietkeanh_tomtat .news_list ul li .hot_news");
        var heightsImgHotNews = imgHotNewshWidth/1.52212;
        for(var i=0;i<heightImgHotNews.length;i++){
            heightImgHotNews[i].style.height = heightsImgHotNews + "px";
        }
    }

    function resizeListcategory() {
        var imgListcategoryhWidth = $('.DsChuyenMucTinBai .news_list_chuyenmuc ul.listcategory li .listcategory__img').width();
        var heightImgListcategory = $(".DsChuyenMucTinBai .news_list_chuyenmuc ul.listcategory li .listcategory__img");
        var heightsImgListcategory = imgListcategoryhWidth/1.52212;
        for(var i=0;i<heightImgListcategory.length;i++){
            heightImgListcategory[i].style.height = heightsImgListcategory + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resizeListcategory", resizeListcategory);
        window.addEventListener("resizeListcategory", resizeListcategory);
        window.onresize = function () {
            resizeHotNews();
            resizeListcategory();
        };
            resizeHotNews();
            resizeListcategory();
    });

    /******************************************************************/

    function resizeTopicTravel() {
        var imgTopicTravelWidth = $('.DsTinBaiLietKeAnh_TieudeCM ul li .group_scoll .img_scrollnews').width();
        var heightImgTopicTravel = $(".DsTinBaiLietKeAnh_TieudeCM ul li .group_scoll .img_scrollnews");
        var heightsImgTopicTravel = imgTopicTravelWidth/1.56;
        for(var i=0;i<heightImgTopicTravel.length;i++){
            heightImgTopicTravel[i].style.height = heightsImgTopicTravel + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resizeTopicTravel", resizeTopicTravel);
        window.onresize = function () {
            resizeTopicTravel();
        };
            resizeTopicTravel();
    });

    /******************************************************************/

    $( "#MainMenu #accordion > .card .collapse" ).each(function() {
		if($(this).children('a').hasClass('active')) {
			$(this).siblings('a').addClass('active');
			$(this).parents().siblings('a').addClass('active');
		}
	});

</script>

<script>
    function resizeMedia() {
        var imgMediaWidth = $('.Thuvien .Specialties-MediaLibrary .Image-gallery .Image-bg-lg .album-lg .image-light a img').width();
        var heightImgMedia = $(".Thuvien .Specialties-MediaLibrary .Image-gallery .Image-bg-lg .album-lg .image-light a img");
        var heightsImgMedia = imgMediaWidth/1.375;
        for(var i=0;i<heightImgMedia.length;i++){
            heightImgMedia[i].style.height = heightsImgMedia + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resizeMedia", resizeMedia);
        window.onresize = function () {
            resizeMedia();
        };
            resizeMedia();
    });
</script>

<script>
    function resizeVideo() {
        var imgVideoWidth = $('.Thuvien .Specialties-MediaLibrary .Video-library .Video-Gallery .lightgallery-video-html5 .videosAlbum .img-responsive').width();
        var heightImgVideo = $(".Thuvien .Specialties-MediaLibrary .Video-library .Video-Gallery .lightgallery-video-html5 .videosAlbum .img-responsive");
        var heightsImgVideo = imgVideoWidth/1.764705882352941;
        for(var i=0;i<heightImgVideo.length;i++){
            heightImgVideo[i].style.height = heightsImgVideo + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resizeVideo", resizeVideo);
        window.onresize = function () {
            resizeVideo();
        };
            resizeVideo();
    });
</script>

<script>
    $(document).ready(function () {
        $('#Dien-gia .owl-carousel').owlCarousel({
            loop:true,
            margin:24,
            autoplay:false,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            dots:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                },
                1024:{
                    items:4
                }
            }
        })
    });
            $(document).ready(function(){

        function height(){
            var widthImg = $(".section-recuitmedia .item .item-img").width();
            var heightBg = $(".section-recuitmedia .item .item-img");
            console.log(widthImg);
            heightBg.height(widthImg/1.39285714286);
            console.log(heightBg);

        };

        window.addEventListener("height", height);
        window.onresize = function () {
            height();

        };
            height();
        });
            $(document).ready(function(){

        function height(){
            var widthImg = $(".section-guide .news_list__first .news_list__first__img a").width();
            var heightBg = $(".section-guide .news_list__first .news_list__first__img a");
            console.log(widthImg);
            heightBg.height(widthImg/1.97916666667);
            console.log(heightBg);

        };

        window.addEventListener("height", height);
        window.onresize = function () {
            height();

        };
            height();
        });
</script>

<!-- <script>
    $(window).on('hashchange', function() {
        let hash = window.location.hash;
        $('.mini-navbar .contentpane .menu-m1 > ul > li > a').closest('.mini-navbar .contentpane .menu-m1 > ul > li > a').removeClass('current');
        $('.mini-navbar .contentpane .menu-m1 > ul > li > a[href=\"' + hash + '\"]').closest('.mini-navbar .contentpane .menu-m1 > ul > li > a').addClass('current');
    });
</script> -->

<script> /* Add current class to Small-navbar */
    let sections = document.querySelectorAll('section.onepage');
    let navLinks = document.querySelectorAll('.mini-navbar .contentpane .menu-m9 > ul > li > a');

    window.onscroll = () =>{
        sections.forEach(sec =>{
            let top = window.scrollY;
            let offset = sec.offsetTop - 10;
            let height = sec.offsetHeight;
            let id = sec.getAttribute('id');

            if (top >= offset && top < offset + height){
                navLinks.forEach(links =>{
                    links.classList.remove('current');
                    document.querySelector('.mini-navbar .contentpane .menu-m9 > ul > li > a[href*=' + id + ']').classList.add('current');
                })
            }
        })
    }
</script>

<script> /* Set display when scroll */
    function setScrollDisplay() { 
    var targetOffset = $("#Gioi-thieu").offset().top;

    var $w = $(window).scroll(function(){
    if ( $w.scrollTop() > targetOffset - 80) {  
        $('.small-navbar').css({"display":"block"});
        $('.header .mini-menubar .overlay-blur').css({"background":"none"});
        $('.header .mini-menubar .overlay-blur').css({"background-image":"url(/Portals/_default/skins/huecit_kph/assets/images/header-fixed.png)"});
        $('.header .mini-menubar .overlay-blur').css({"background-repeat":"no-repeat"});
        $('.header .mini-menubar .overlay-blur').css({"background-size":"100% 100%"});
    } else {
        $('.small-navbar').css({"display":"none"});
        $('.header .mini-menubar .overlay-blur').css({"background":"none"});
        $('.header .mini-menubar .overlay-blur').css({"background":"linear-gradient(180deg, rgba(0, 0, 0, 0.8) 0%, rgba(0, 0, 0, 0.4) 52.08%, rgba(0, 0, 0, 0) 96.35%)"});
    }
});
};
    setTimeout(()=>{
        setScrollDisplay();
    }, 1000);
</script>

<script> /* Remove Hash when click on Nav bar */
    $(document).ready(function(){
        const menuBtn = $('.mini-navbar .contentpane .menu-m9 > ul > li > a');

        menuBtn.click(()=>{
            setTimeout(()=>{
                removeHash();
            }, 5);
        });

    function removeHash(){
        history.replaceState('', document.title, window.location.origin + window.location.pathname + window.location.search);
    }
    });
</script>

