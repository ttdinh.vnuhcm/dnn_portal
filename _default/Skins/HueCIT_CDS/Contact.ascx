<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<!--#include file="layouts/default/_includes-top.ascx" -->

<!-- Start Header Section -->

<header class="header content-layout contact-page">
    <div class="menu-bar">
        <div class="overlay-blur"></div>
        <div class="container">
            <!--#include file="layouts/default/nav-bar.ascx" -->
        </div>
    </div>
    <!-- ./navbar -->
</header><!-- ./Header -->

<section>
    <div class="breadcrumb">
        <div class="container">
            <dnn:Breadcrumb runat="server" RootLevel="-1" Separator="<i class='material-icons-outlined'>chevron_right</i>" CssClass="object_breadcrumb" id="dnnBreadcrumb"></dnn:Breadcrumb>
        </div>
    </div>
    <div id="ContentPane" runat="server"></div>
    <div class="container-contact">
        <div class="row">
            <div id="Row_Contact_Col8" class="col-lg-12 col-md-12" runat="server"></div>
            <!-- <div id="Row_Contact_Col4" class="col-lg-4 col-md-5" runat="server"></div> -->
        </div>
    </div>
</section>

<section class="section-null">
    <div class="container">
        <div class="row">
            <div id="LienketPane" class="col-12" runat="server"></div>
        </div>
    </div>
</section>
<section class="section-null">
    <div class="container">
        <div class="row">
            <div id="LienketPane_1" class="col-12" runat="server"></div>
        </div>
    </div>
</section>
<section class="section-null">
    <div class="row">
        <div id="LienketPane_2" class="col-12" runat="server"></div>
    </div>
</section>

<!-- Start Footer -->
<footer>
    <div class="footer-top">
        <div class="container">
            <!--#include file="layouts/default/footer-main.ascx" -->
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <!--#include file="layouts/default/footer-legal.ascx" -->
        </div>
    </div>
</footer><!-- ./Footer -->

<!--#include file="layouts/default/search-modal.ascx" -->

<!-- Go To Top Link -->
<div class="back-to-top">
    <a href="#"></a>
</div>

<!--#include file="layouts/default/_includes-bottom.ascx" -->

<script type="text/javascript">
    $('.btn-sidebar').click(function(){
      $("#sidebar").toggleClass("show")
    })

    $('.btn-sidebar-close').click(function(){
      $("#sidebar").toggleClass("show")
    })
</script>