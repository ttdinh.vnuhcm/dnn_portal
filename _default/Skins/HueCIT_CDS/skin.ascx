﻿<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<!--#include file="layouts/default/_includes-top.ascx" -->

<!-- Start Header Section -->
<header class="header">
    <div class="menu-bar">
        <div class="overlay-blur"></div>
        <div class="container">
            <!--#include file="layouts/default/nav-bar.ascx" -->
        </div>
    </div>
    <!-- ./navbar -->
</header><!-- ./Header -->

<!-- Start Slider area -->
<div class="slide-areas">
    <div id="sliderPane" runat="server"></div>
    <div id="WeatherPane" runat="server"></div>
</div>

<!-- ./Slider area -->
<section class="notification bg-body-2">
    <div class="container">
        <div id="NotificationPane" runat="server"></div>
    </div>
</section>

<section class="bg-body-2 topic cm">
    <div class="first-culture">
        <div class="container">
            <div class="container-item first-culture">
                <div class="row row-6-6">
                    <div class="col-md-6">
                        <div class="row-custom">
                            <div id="LeftColPane_1A" class="col-custom col-custom-1" runat="server"></div>
                            <div id="RightColPane_1A" class="class-js-1a col-custom col-custom-2" runat="server"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row-custom">
                            <div id="LeftColPane_1B" class="col-custom col-custom-1" runat="server"></div>
                            <div id="RightColPane_1B" class="class-js-1b col-custom col-custom-2" runat="server"></div>
                        </div>
                    </div>
                </div>
                <div class="row row-6-6">
                    <div class="col-md-6">
                        <div class="row-custom">
                            <div id="LeftColPane_2A" class="col-custom col-custom-1" runat="server"></div>
                            <div id="RightColPane_2A" class="class-js-2a col-custom col-custom-2" runat="server"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row-custom">
                            <div id="LeftColPane_2B" class="col-custom col-custom-1" runat="server"></div>
                            <div id="RightColPane_2B" class="class-js-2b col-custom col-custom-2" runat="server"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="first-travel">
        <div class="container">
            <div class="container-item">
                <div class="row row-4-6">
                    <div class="col-md-4">
                        <div id="LeftColPane_1_Row4"  runat="server"></div>
                    </div>
                    <div class="row col-md-8 tour-pane">
                        <div id="RightColPane_1_Row4" class="col-12" runat="server"></div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div id="RightColPane_1_Row6A" runat="server"></div>
                            </div>
                            <div class="col-md-6">
                                <div id="RightColPane_2_Row6A" runat="server"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="RightColPane_1_Row6B" runat="server"></div>
                            </div>
                            <div class="col-md-6">
                                <div id="RightColPane_2_Row6B" runat="server"></div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="first-hue24h">
        <div class="container">
            <div class="hue24h-event">
                <div class="container-item">
                    <div class="row">
                        <div id="RightColPane_1_Row12" class="col-12" runat="server"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="first-Digital">
        <div class="container">
            <div class="container-item">
                <div class="row">
                    <div id="ColPane_Row12" class="enterprise-hot noTitleCM col-12" runat="server"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="first-Library-Digital">
        <div class="container">
            <div class="container-item">
                <div class="row">
                    <div id="ColPaneFull" class="col-12" runat="server"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-celebration">
    <div class="container">
        <div class="row">
            <div class="col-12" id="Celebration_Full">
                <div class="menuCelebration topic-menuList" id="menulist" runat="server"></div>
            </div>
            <div class="group-celebration">
                <div class="col-md-3 hotnews" id="Celebration_1" runat="server"></div>
                <div class="col-md-3 hotnews" id="Celebration_2" runat="server"></div>
                <div class="col-md-3 hotnews" id="Celebration_3" runat="server"></div>
                <div class="col-md-3 hotnews" id="Celebration_4" runat="server"></div>
            </div>
        </div>
    </div>
</section>

<section class="section-people-Hue">
    <div class="container">
        <div class="row info-des">
            <div class="col-12 hotnews noCM noTitleDes" id="People_Content" runat="server"></div>
        </div>
    </div>
</section>

<section class="section-media">
    <div class="container">
        <div class="row">
            <div class="col-12" id="Media_Content" runat="server"></div>
        </div>
    </div>
</section>

<section class="section-content-2-columm">
    <div class="container">
        <div class="row info-des noTitleDes">
            <div class="col-md-6" id="content_col_1" runat="server"></div>
            <div class="col-md-6" id="content_col_2" runat="server"></div>
        </div>
    </div>
</section>

<section class="section-content-2-columm-sidebar">
    <div class="container">
        <div class="row">
            <div class="col-md-3 noTitleCM noTitleDes" id="Left_Sidebar" runat="server"></div>
            <div class="col-md-9 noTitleCM noTitleDes" id="Right_Content" runat="server"></div>
        </div>
    </div>
</section>

<section class="section-content-full">
    <div class="container">
        <div class="row">
            <div class="col-12 hotnews noCM container-nocm content-full" id="content_full" runat="server"></div>
        </div>
    </div>
</section>

<section class="section-food">
    <%--<div class="bg-food">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="menuCM_list title-orange" id="menuFood" runat="server"></div>
                <div class="hot_list" id="hot_food" runat="server"></div>
            </div>
        </div>
    </div>

    <div class="bg-travel">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="menuCM_list title-orange" id="menuTravel" runat="server"></div>
                <div class="hot_list" id="hot_Travel" runat="server"></div>
            </div>
        </div>
    </div>--%>

    <div class="bg-final-travel">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12" id="final_travel_full" runat="server"></div>
                <div class="silde-2-column col-12" id="final_travel" runat="server"></div>
            </div>
        </div>
    </div>
</section>

<section class="section-culture">
    <div class="container">
        <div class="row">
            <div class="col-12" id="Culture_Pane_Full" runat="server"></div>
            <div class="col-md-6" id="Culture_Pane_1" runat="server"></div>
            <div class="col-md-6" id="Culture_Pane_2" runat="server"></div>
        </div>
    </div>
</section>

<div id="content-areas">
    <div class="container">
        <div class="row"><div class="col-md-12"><div id="ContentPane" runat="server" /></div></div>
    </div><!-- End : Content Pane -->
</div> <!-- ./content-areas -->

<!-- Start Footer -->
<footer>
    <div class="footer-top">
        <div class="container">
            <!--#include file="layouts/default/footer-main.ascx" -->
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <!--#include file="layouts/default/footer-legal.ascx" -->
        </div>
    </div>
</footer><!-- ./Footer -->

<!--#include file="layouts/default/search-modal.ascx" -->

<!-- Go To Top Link -->
<div class="back-to-top">
    <a href="#"></a>
</div>

<!--#include file="layouts/default/_includes-bottom.ascx" -->

<script type="text/javascript">
    $(document).ready(function () {
        $("#dnn_sliderPane #owl-slideQC").owlCarousel({
            items: 1,
            loop: true,
            margin: 0,
            autoplay: true,
            dots: false,
            nav: false,
            navText: ["<i class='material-icons-outlined'>chevron_left</i>", "<i class='material-icons-outlined'>chevron_right</i>"],
            animateOut: 'fadeOut',
            animateIn: 'fadeIn'

        });
        // Wrap around nav & dots
        $("#dnn_sliderPane #owl-slideQC").each(function(index) {
            $(this).find('.owl-nav button').wrapAll("<div class='container'></div>");
        });

        // $(function(){
        //     var container = $('<div class="container"><button type="button" role="presentation" class="owl-prev"><i class="material-icons-outlined">chevron_left</i></button><button type="button" role="presentation" class="owl-next"><i class="material-icons-outlined">chevron_right</i></button></div>');

        //     $('#owl-slideQC .owl-nav').html(container);
        // });

        $(".class-js-1a #owl-ImageNgangBG").owlCarousel({
            items: 1,
            loop: true,
            margin: 0,
            autoplay: true,
            dots: false,
            nav: false,
            navText: ["<i class='material-icons-outlined'>chevron_left</i>", "<i class='material-icons-outlined'>chevron_right</i>"],
            animateOut: 'fadeOut',
            animateIn: 'fadeIn'

        });
        $(".class-js-1b #owl-ImageNgangBG").owlCarousel({
            items: 1,
            loop: true,
            margin: 0,
            autoplay: true,
            dots: false,
            nav: false,
            navText: ["<i class='material-icons-outlined'>chevron_left</i>", "<i class='material-icons-outlined'>chevron_right</i>"],
            animateOut: 'fadeOut',
            animateIn: 'fadeIn'

        });
        $(".class-js-2a #owl-ImageNgangBG").owlCarousel({
            items: 1,
            loop: true,
            margin: 0,
            autoplay: true,
            dots: false,
            nav: false,
            navText: ["<i class='material-icons-outlined'>chevron_left</i>", "<i class='material-icons-outlined'>chevron_right</i>"],
            animateOut: 'fadeOut',
            animateIn: 'fadeIn'

        });
        $(".class-js-2b #owl-ImageNgangBG").owlCarousel({
            items: 1,
            loop: true,
            margin: 0,
            autoplay: true,
            dots: false,
            nav: false,
            navText: ["<i class='material-icons-outlined'>chevron_left</i>", "<i class='material-icons-outlined'>chevron_right</i>"],
            animateOut: 'fadeOut',
            animateIn: 'fadeIn'

        });

        $(".silde-2-column #owl-ImageNgang2CotBG").owlCarousel({
            //navigation : false, // Show next and prev buttons
            loop: true,
            margin: 30,
            slideSpeed: 500,
            lazyEffect: "fade",
            //autoPlay: 5000,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            paginationSpeed: 400,
            paginationNumbers: false,
            singleItem: true,
            transitionStyle: "fade",
            pagination:true,
            dots: false,
            nav: true,
            navText: ["<img src='/Portals/_default/skins/huecit_kph/assets/images/prev-slide.png'>","<img src='/Portals/_default/skins/huecit_kph/assets/images/next-slide.png'>"],
            responsive:{
                0:{
                    items:1
                },
                992:{
                    items:2
                }
            }
        });

    });

    function resize() {
        var imgWidth = $('.slide-areas #owl-slideQC .item .img_bg').width();
        var heightimg = $(".slide-areas #owl-slideQC .item .img_bg");
        var heights = imgWidth/3.437;
        for(var i=0;i<heightimg.length;i++){
            heightimg[i].style.height = heights + "px";
        }


    }
    $(document).ready(function () {
        window.addEventListener("resize", resize);
        window.onresize = function () {
            resize();
        };
            resize();
    });

    /******************************************************************/
    function resizeHotNews() {
        var imgHotNewshWidth = $('.hotnews .dstinbailietkeanh_tomtat .news_list ul li .hot_news').width();
        var heightImgHotNews = $(".hotnews .dstinbailietkeanh_tomtat .news_list ul li .hot_news");
        var heightsImgHotNews = imgHotNewshWidth/1.52212;
        for(var i=0;i<heightImgHotNews.length;i++){
            heightImgHotNews[i].style.height = heightsImgHotNews + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resizeHotNews", resizeHotNews);
        window.onresize = function () {
            resizeHotNews();
        };
            resizeHotNews();
    });

    /******************************************************************/
    function resizeHeightBackground() {
        var leftColPane1A = $('#dnn_LeftColPane_1A').innerHeight();
        var leftColPane1B = $('#dnn_LeftColPane_1B').innerHeight();
        var leftColPane2A = $('#dnn_LeftColPane_2A').innerHeight();
        var leftColPane2B = $('#dnn_LeftColPane_2B').innerHeight();

        var heightBackgroundOwlCarousel = $('.first-culture #owl-ImageNgangBG .item .slide_news .slide-image img');
        var heightMax = Math.max( leftColPane1A, leftColPane1B, leftColPane2A, leftColPane2B);
        heightBackgroundOwlCarousel.height(heightMax);
    }

    $(document).ready(function () {
        window.addEventListener("resizeHeightBackground", resizeHeightBackground);
        window.onresize = function () {
            resizeHeightBackground();
        };
            resizeHeightBackground();
    });

    /******************************************************************/

    function resizeHotNewsTravel() {
        var imgHotNewsTravelWidth = $('.tour-pane .news_list ul li .hot_news').width();
        var heightImgHotNewsTravel = $(".tour-pane .news_list ul li .hot_news");
        var heightsImgHotNewsTravel = imgHotNewsTravelWidth/1.58208955224;
        for(var i=0;i<heightImgHotNewsTravel.length;i++){
            heightImgHotNewsTravel[i].style.height = heightsImgHotNewsTravel + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resizeHotNewsTravel", resizeHotNewsTravel);
        window.onresize = function () {
            resizeHotNewsTravel();
        };
            resizeHotNewsTravel();
    });

    /******************************************************************/

    function resizeItemsHot() {
        var itemsHotWidth = $('.hot_list ul li').width();
        var heightItemsHot = $(".hot_list ul li");
        var heightMenuItemsHot = $(".menuCM_list .DnnModule-DNNTrinhDienChuyenMuc");
        var heightsItemsHot = itemsHotWidth/0.66666666666;
        for(var i=0;i<heightItemsHot.length;i++){
            heightItemsHot[i].style.height = heightsItemsHot + "px";
        }

        heightMenuItemsHot.innerHeight(heightsItemsHot);
    }

    $(document).ready(function () {
        window.addEventListener("resizeItemsHot", resizeItemsHot);
        window.onresize = function () {
            resizeItemsHot();
        };
            resizeItemsHot();
    });

    /******************************************************************/
	function resizeTravelKnow() {
		var imgTravelKnowWidth = $('#dnn_Culture_Pane_1 .DnnModule-DNN_HTML .travel-left-pane .item-you-need-know .item-image').innerWidth();
		var heightImgTravelKnow = $("#dnn_Culture_Pane_1 .DnnModule-DNN_HTML .travel-left-pane .item-you-need-know .item-image");
		var heightsImgTravelKnow = imgTravelKnowWidth;
		for(var i=0;i<heightImgTravelKnow.length;i++){
			heightImgTravelKnow[i].style.height = heightsImgTravelKnow + "px";
		}
	}
		
	function resizeTravelRight() {
		var imgTravelLeftHeight = $('#dnn_Culture_Pane_1 .DnnModule-DNN_HTML .travel-left-pane').innerHeight();
		var heightTitleTravelRight = $("#dnn_Culture_Pane_2 .DnnModule-DNN_HTML .map-relic .title-map").outerHeight(true);
		var heightImgTravelRight = $("#dnn_Culture_Pane_2 .DnnModule-DNN_HTML .map-relic .image-maps");
		var heightsImgTravelRight = imgTravelLeftHeight - (heightTitleTravelRight + 50);
		heightImgTravelRight.height(heightsImgTravelRight);
	}

	$(document).ready(function () {
		window.addEventListener("resizeTravelKnow", resizeTravelKnow);
		window.addEventListener("resizeTravelRight", resizeTravelRight);
		window.onresize = function () {
			resizeTravelKnow();
			resizeTravelRight();
		};
			resizeTravelKnow();
			resizeTravelRight();
	});

	/******************************************************************/

    function resizeTopicTravel() {
        var imgTopicTravelWidth = $('.info-des .DsTinBaiLietKeAnh_TieudeCM ul li .group_scoll .img_scrollnews').width();
        var heightImgTopicTravel = $(".info-des .DsTinBaiLietKeAnh_TieudeCM ul li .group_scoll .img_scrollnews");
        var heightsImgTopicTravel = imgTopicTravelWidth/1.56;
        for(var i=0;i<heightImgTopicTravel.length;i++){
            heightImgTopicTravel[i].style.height = heightsImgTopicTravel + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resizeTopicTravel", resizeTopicTravel);
        window.onresize = function () {
            resizeTopicTravel();
        };
            resizeTopicTravel();
    });

    /******************************************************************/

    function resizeSuggestTravel() {
        var imgSuggestTravelWidth = $('.section-content-2-columm .DsTinBaiLietKeAnh_TieudeCM ul li:not(:first-child) .group_scoll .img_scrollnews').width();
        var heightImgSuggestTravel = $(".section-content-2-columm .DsTinBaiLietKeAnh_TieudeCM ul li:not(:first-child) .group_scoll .img_scrollnews");
        var heightsImgSuggestTravel = imgSuggestTravelWidth/1.69285714286;
        for(var i=0;i<heightImgSuggestTravel.length;i++){
            heightImgSuggestTravel[i].style.height = heightsImgSuggestTravel + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resizeSuggestTravel", resizeSuggestTravel);
        window.onresize = function () {
            resizeSuggestTravel();
        };
            resizeSuggestTravel();
    });

    /******************************************************************/

    function resizeDigitalHue24h() {
        var imgDigitalHue24hWidth = $('.module_container_news3img .img_left_large .group_scoll .img_scrollnews').width();
        var heightImgDigitalHue24h = $(".module_container_news3img .img_left_large .group_scoll .img_scrollnews");
        var heightsImgDigitalHue24h = imgDigitalHue24hWidth/1.91666666667;
        for(var i=0;i<heightImgDigitalHue24h.length;i++){
            heightImgDigitalHue24h[i].style.height = heightsImgDigitalHue24h + "px";
        }
    }

    function resizeRightDigitalHue24h() {
        var imgRightDigitalHue24hWidth = $('.module_container_news3img .img_right_list .group_scoll .img_scrollnews').width();
        var heightImgRightDigitalHue24h = $(".module_container_news3img .img_right_list .group_scoll .img_scrollnews");
        var heightsImgRightDigitalHue24h = imgRightDigitalHue24hWidth/1.62142857143;
        for(var i=0;i<heightImgRightDigitalHue24h.length;i++){
            heightImgRightDigitalHue24h[i].style.height = heightsImgRightDigitalHue24h + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resizeDigitalHue24h", resizeDigitalHue24h);
        window.addEventListener("resizeRightDigitalHue24h", resizeRightDigitalHue24h);
        window.onresize = function () {
            resizeDigitalHue24h();
            resizeRightDigitalHue24h();
        };
            resizeDigitalHue24h();
            resizeRightDigitalHue24h();
    });

    /******************************************************************/

    function resizeHue24hEvent() {
		var imgHue24hEventWidth = $('.hue24h-event ul li .event-item .image-event').width();
		var heightImgHue24hEvent = $(".hue24h-event ul li .event-item .image-event");
		var heightsImgHue24hEvent = imgHue24hEventWidth/(530/335);
		for(var i=0;i<heightImgHue24hEvent.length;i++){
			heightImgHue24hEvent[i].style.height = heightsImgHue24hEvent + "px";
		}
	}

	$(document).ready(function () {
		window.addEventListener("resizeHue24hEvent", resizeHue24hEvent);
		window.onresize = function () {
			resizeHue24hEvent();
		};
			resizeHue24hEvent();
	});

    /******************************************************************/

    function resizeFirstEnterprise() {
		var imgFirstEnterpriseWidth = $('.first-Digital .enterprise-hot ul li .group_scoll .img_scrollnews').width();
		var heightImgFirstEnterprise = $(".first-Digital .enterprise-hot ul li .group_scoll .img_scrollnews");
		var heightsImgFirstEnterprise = imgFirstEnterpriseWidth/1.56;
		for(var i=0;i<heightImgFirstEnterprise.length;i++){
			heightImgFirstEnterprise[i].style.height = heightsImgFirstEnterprise + "px";
		}
	}

	$(document).ready(function () {
		window.addEventListener("resizeFirstEnterprise", resizeFirstEnterprise);
		window.onresize = function () {
			resizeFirstEnterprise();
		};
			resizeFirstEnterprise();
	});

    /******************************************************************/

    function resizeRightContentEnterprise() {
		var imgRightContentEnterpriseWidth = $('.section-content-2-columm-sidebar #dnn_Right_Content .DsTinBaiLietKeAnh_TieudeCM ul li .img_scrollnews').width();
		var heightImgRightContentEnterprise = $(".section-content-2-columm-sidebar #dnn_Right_Content .DsTinBaiLietKeAnh_TieudeCM ul li .img_scrollnews");
		var heightsImgRightContentEnterprise = imgRightContentEnterpriseWidth/1.69565217391;
		for(var i=0;i<heightImgRightContentEnterprise.length;i++){
			heightImgRightContentEnterprise[i].style.height = heightsImgRightContentEnterprise + "px";
		}
	}

	$(document).ready(function () {
		window.addEventListener("resizeRightContentEnterprise", resizeRightContentEnterprise);
		window.onresize = function () {
			resizeRightContentEnterprise();
		};
			resizeRightContentEnterprise();
	});

    /******************************************************************/

    function resizeLeftSidebarEnterprise() {
		var imgLeftSidebarEnterpriseWidth = $('.section-content-2-columm-sidebar #dnn_Left_Sidebar .DsTinBaiLietKeAnh_TieudeCM ul li .img_scrollnews').width();
		var heightImgLeftSidebarEnterprise = $(".section-content-2-columm-sidebar #dnn_Left_Sidebar .DsTinBaiLietKeAnh_TieudeCM ul li .img_scrollnews");
		var heightsImgLeftSidebarEnterprise = imgLeftSidebarEnterpriseWidth/1.5;
		for(var i=0;i<heightImgLeftSidebarEnterprise.length;i++){
			heightImgLeftSidebarEnterprise[i].style.height = heightsImgLeftSidebarEnterprise + "px";
		}
	}

	$(document).ready(function () {
		window.addEventListener("resizeLeftSidebarEnterprise", resizeLeftSidebarEnterprise);
		window.onresize = function () {
			resizeLeftSidebarEnterprise();
		};
			resizeLeftSidebarEnterprise();
	});

	/******************************************************************/

    function resize2ColumnCultureHeight() {
        var img2ColumnCultureWidth = $('.section-culture .dstinbaimotanh ul li .news_list__first .news_list__first__img').width();
        var heightImg2ColumnCultureHeight = $(".section-culture .dstinbaimotanh ul li .news_list__first .news_list__first__img");
        var heightsImg2ColumnCultureHeight = img2ColumnCultureWidth/1.63636363636;
        for(var i=0;i<heightImg2ColumnCultureHeight.length;i++){
            heightImg2ColumnCultureHeight[i].style.height = heightsImg2ColumnCultureHeight + "px";
        }
    }

    function resize2ColumnTravelHeight() {
        var img2ColumnTravelWidth = $('.section-content-2-columm .DsTinBaiLietKeAnh_TieudeCM ul li:first-child .group_scoll .img_scrollnews').width();
        var heightImg2ColumnTravelHeight = $(".section-content-2-columm .DsTinBaiLietKeAnh_TieudeCM ul li:first-child .group_scoll .img_scrollnews");
        var heightsImg2ColumnTravelHeight = img2ColumnTravelWidth/1.63636363636;
        for(var i=0;i<heightImg2ColumnTravelHeight.length;i++){
            heightImg2ColumnTravelHeight[i].style.height = heightsImg2ColumnTravelHeight + "px";
        }
    }

    $(document).ready(function () {
        window.addEventListener("resize2ColumnCultureHeight", resize2ColumnCultureHeight);
        window.addEventListener("resize2ColumnTravelHeight", resize2ColumnTravelHeight);
        window.onresize = function () {
            resize2ColumnCultureHeight();
            resize2ColumnTravelHeight();
        };
            resize2ColumnCultureHeight();
            resize2ColumnTravelHeight();
    });

    /******************************************************************/

    function resizeTravelFinal() {
		var imgTravelFinalWidth = $('.silde-2-column .tinbai_slide_bg .owl-stage-outer .owl-stage .owl-item .item .group-slide .item-img').width();
		var heightImgTravelFinal = $(".silde-2-column .tinbai_slide_bg .owl-stage-outer .owl-stage .owl-item .item .group-slide .item-img");
		var heightsImgTravelFinal = imgTravelFinalWidth/1.29;
		for(var i=0;i<heightImgTravelFinal.length;i++){
			heightImgTravelFinal[i].style.height = heightsImgTravelFinal + "px";
		}
	}

	$(document).ready(function () {
		window.addEventListener("resizeTravelFinal", resizeTravelFinal);
		window.onresize = function () {
			resizeTravelFinal();
		};
			resizeTravelFinal();
	});

    /******************************************************************/

    function resizeHotDigital() {
		var imgHotDigitalWidth = $('.section-content-2-columm .container .row > div .DnnModule-DNNTrinhDienTinBai .dstinbailietkeanh_tomtat ul li .hot_news').width();
		var heightImgHotDigital = $(".section-content-2-columm .container .row > div .DnnModule-DNNTrinhDienTinBai .dstinbailietkeanh_tomtat ul li .hot_news");

        /*******************************/
        var imgMediaDigitalWidth = $('.section-content-2-columm .Image-bg-lg ul li .album-lg .image-light').width();
		var heightImgMediaDigital = $(".section-content-2-columm .Image-bg-lg ul li .album-lg .image-light");

		var heightsImgHotDigital = imgHotDigitalWidth/1.69285714286;
		for(var i=0;i<heightImgHotDigital.length;i++){
			heightImgHotDigital[i].style.height = heightsImgHotDigital + "px";
            heightImgMediaDigital[i].style.height = heightsImgHotDigital + "px";
		}
	}

	$(document).ready(function () {
		window.addEventListener("resizeHotDigital", resizeHotDigital);
		window.onresize = function () {
			resizeHotDigital();
		};
			resizeHotDigital();
	});

	/******************************************************************/

    function resizeVR3D() {
        var imgVR3DWidth = $(".vr3d-img").width();
        var heightImgVR3D = $(".vr3d-img");

        heightImgVR3D.height(imgVR3DWidth);
    };

    $(document).ready(function () {
		window.addEventListener("resizeVR3D", resizeVR3D);
		window.onresize = function () {
			resizeVR3D();
		};
			resizeVR3D();
	});

</script>

<script>!(function () {
  let e = document.createElement("script"),
    t = document.head || document.getElementsByTagName("head")[0];
  (e.src = "/Portals/_default/skins/huecit_kph/assets/js/rasa-webchat@1.x.x.js"),
    // Replace 1.x.x with the version that you want
    (e.async = !0),
    (e.onload = () => {
      window.WebChat.default(
        { 
            tooltipPayload: "/chaohoi",
            tooltip: true,
            tooltipDelay: 500,
            customData: { language: "en" },
            socketUrl: "http://20.172.29.221/",
            socketPath: "/socket.io/",
            //title: "Khám Phá Huế",
            //subtitle: "Mang Huế đến với mọi người",
            inputTextFieldHint: "Mời bạn nhập vào",
            embedded: false,
            showFullScreenButton: true,
            showMessageDate: true,
            displayUnreadCount: false,
            profileAvatar: "http://kph2022.huecit.com/Portals/0/Logo_vi.png?ver=2022-06-29-160705-883",
            params: {"storage": "session"}
            // add other props here
        },
        null
      );
    }),
    t.insertBefore(e, t.firstChild);
})();
</script>