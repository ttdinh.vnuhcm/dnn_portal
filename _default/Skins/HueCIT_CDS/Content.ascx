<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<!--#include file="layouts/default/_includes-top.ascx" -->

<!-- Start Header Section -->

<header class="header content-layout">
    <div class="menu-bar">
        <div class="overlay-blur"></div>
        <div class="container">
            <!--#include file="layouts/default/nav-bar.ascx" -->
        </div>
    </div>
    <!-- ./navbar -->
</header><!-- ./Header -->
<div class="group-content-areas">
    <section class="container-content">
        <div class="breadcrumb">
            <div class="container">
                <dnn:Breadcrumb runat="server" RootLevel="-1" Separator="<i class='material-icons-outlined'>/</i>" CssClass="object_breadcrumb" id="dnnBreadcrumb"></dnn:Breadcrumb>
            </div>
        </div>

        <div class="container-content">
            <div class="container">
                <div class="row">
                    <main id="sidebar" class="col-lg-3 col-md-4">
                        <a title="Close sidebar" role="button" class="btn-sidebar-close"><i class="material-icons-outlined">close</i></a>
                        <div class="row row-offcanvas row-offcanvas-left HueCIT_NoTitle_Content_Detail">
                            <div id="Row1_LeftPane" runat="server" class="col-md-12 hotnews noCM group-3"></div>
                            <div id="Row2_LeftPane" runat="server" class="col-md-12 hotnews group-3"></div>
                            <div id="Row3_LeftPane" runat="server" class="col-md-12 hotnews group-3"></div>                        
                            <div class="col-md-12 lit-art">
                                <div class="lit-art topic-menuList" id="lit_Art" runat="server"></div>
                                <div id="Row4_LeftPane" runat="server" class="hotnews group-3"></div>
                            </div>
                        </div>

                    </main>
                    <div id="GridLeft" class="col-lg-9 col-md-8">
                        <a title="Sidebar right" class="btn-sidebar" role="button"><span class="material-icons-outlined">menu_open</span></a>
                        <div class="row">
                            <div id="Row_RightPane" runat="server" class="col-md-12 most_read"></div>
                            <div id="Row_RightPane2" runat="server" class="col-md-12 most_read2"></div>
                            <div id="Row_RightPane_Many_CM" runat="server" class="col-md-12 most_read noNewsLink"></div>
                            <div id="Row_RightPane_Accordion_NoIco" runat="server" class="col-md-12 most_read noNewsLink"></div>
                            <div id="Row_RightPane_Accordion_Ico" runat="server" class="col-md-12 most_read noNewsLink"></div>
                            <div id="Row_RightPane_ViDienTu" runat="server" class="col-md-12 most_read noNewsLink"></div>
                            <div id="Row_RightPane_KhoaHocTieuBieu" runat="server" class="col-md-12 most_read noNewsLink"></div>
                            <div id="Row_RightPane_ThongBaoCanhbao" runat="server" class="col-md-12 noNewsLink"></div>
                        </div>
                    </div>
                    <div id="RowFull_LeftPane" runat="server" class="col-md-12 hotnews group-3"></div>
                </div>
            </div>
        </div>
    </section>

    <div id="content-areas">

        <div class="container">
            <div class="row"><div class="col-md-12"><div id="ContentPane" runat="server" /></div></div>
        </div><!-- End : Content Pane -->

    </div> <!-- ./content-areas -->
</div>
<!-- Start Footer -->
<footer>
    <div class="footer-top">
        <div class="container">
            <!--#include file="layouts/default/footer-main.ascx" -->
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <!--#include file="layouts/default/footer-legal.ascx" -->
        </div>
    </div>
</footer><!-- ./Footer -->

<!--#include file="layouts/default/search-modal.ascx" -->

<!-- Go To Top Link -->
<div class="back-to-top">
    <a href="#"></a>
</div>

<!--#include file="layouts/default/_includes-bottom.ascx" -->

<script type="text/javascript">
    $('.btn-sidebar').click(function(){
      $("#sidebar").toggleClass("show")
    })

    $('.btn-sidebar-close').click(function(){
      $("#sidebar").toggleClass("show")
    })

</script>


<script type="text/javascript">
    function resize() {

        var imgListcategoryhWidth = $('.DsChuyenMucTinBai .news_list_chuyenmuc ul.listcategory li .listcategory__img').width();
        var heightImgListcategory = $('.DsChuyenMucTinBai .news_list_chuyenmuc ul.listcategory li .listcategory__img');
        var heightsImgListcategory = imgListcategoryhWidth/1.54368932039;
        for(var i=0;i<heightImgListcategory.length;i++){
            heightImgListcategory[i].style.height = heightsImgListcategory + "px";
        }

        var imgListcategoryhWidth1 = $('.ModHueTourisEventViewOrtherC .other_events .Orther-img ').width();
        var heightImgListcategory1 = $('.ModHueTourisEventViewOrtherC .other_events .Orther-img ');
        var heightsImgListcategory1 = imgListcategoryhWidth1/1.53883495146;
        for(var i=0;i<heightImgListcategory1.length;i++){
            heightImgListcategory1[i].style.height = heightsImgListcategory1 + "px";
        }

        var widthImgViDienTu = $('.ChuyenMucTabContent .tab-content .tab_pane_image img').width();
        var heightImgViDienTu = $('.ChuyenMucTabContent .tab-content .tab_pane_image img');
        var heightsImgViDienTu = widthImgViDienTu/1.54368932039;
        for(var i=0;i<heightImgViDienTu.length;i++){
            heightImgViDienTu[i].style.height = heightsImgViDienTu + "px";
        }

        var widthImgdtTrucTuyen = $('.DsTinBaiAnh_SlideNgangBG1 .slide-image .news_thumb').width();
        var heightImgdtTrucTuyen = $('.DsTinBaiAnh_SlideNgangBG1 .slide-image .news_thumb');
        var heightsImgdtTrucTuyen = widthImgdtTrucTuyen/1.6;
        for(var i=0;i<heightImgdtTrucTuyen.length;i++){
            heightImgdtTrucTuyen[i].style.height = heightsImgdtTrucTuyen + "px";
        }

        // var widthImgdtKhTieuBieu = $('.DsTinBaiAnh_SlideNgangBG1 .owl-stage-outer .item').width();
        // var heightImgdtKhTieuBieu = $('.DsTinBaiAnh_SlideNgangBG1 .owl-stage-outer .item');
        // var heightsImgdtKhTieuBieu = widthImgdtKhTieuBieu/0.64430379746;
        // for(var i=0;i<heightImgdtKhTieuBieu.length;i++){
        //     heightImgdtKhTieuBieu[i].style.height = heightsImgdtKhTieuBieu + "px";
        // }

    }
    $(document).ready(function () {
        $('#dnn_Row_RightPane_KhoaHocTieuBieu .owl-theme').owlCarousel({
            loop:true,
            //margin:24,
            autoplay:false,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            nav:true,
            navText: ["<img src='/Portals/_default/Skins/HueCIT_CDS/assets/images/btn-Previous.png'>", "<img src='/Portals/_default/Skins/HueCIT_CDS/assets/images/btn-Next.png'>"],
            dots:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                },
                1024:{
                    items:3
                },
                1080:{
                    items:3
                }
            }
        })
        window.addEventListener("resize", resize);
        window.onresize = function () {
            resize();
        };
            resize();
    });


    var
        Titlemain = $('.news_list_chuyenmuc > .container_module_title > .circle > a').text();
        Titlemain2 = $('.news_list_detail > .container_module_title > .circle > a').text();
        lengths = $('.container-content .breadcrumb .container > span > span > span').length;
        if(lengths >=4){
            Titlemain3 = $('.container-content .breadcrumb .container > span > span > span:nth-of-type(3) span').text();
        } else{
            Titlemain3 = $('.container-content .breadcrumb .container > span > span > span:last-child a').text();
           
        }
      var  Titlemain4 = $('.container-content .breadcrumb .container > span > span > span:last-child a').text();
      $("#MainMenu > .accordion > .card > .collapse >a").each(function() {
        if($(this).text()==Titlemain4){
            $(this).addClass('active');
        }
      })

    $('#MainMenu > .accordion > .card').each(function(){
        if ($(this).children('a').text() == Titlemain) {
            $(this).addClass('active');
        }

        if ($(this).children('a').text() == Titlemain2) {
            $(this).addClass('active');
        }
        if ($(this).children('a').text() == Titlemain3) {
            $(this).removeClass('active');
            $(this).addClass('active');
        }
    });

   
    var lengths1 = $('.container-content .breadcrumb .container > span > span > span > span').text();
    
        
    $('#main-menu > li > a').each(function(){
        if ($(this).text() == lengths1) {
            $(this).removeClass('current');
            $(this).addClass('current');
        }
    });

    $(document).ready(function(){
    $.fn.accordion = function() {
      const trigger = $(this).find('.accordion-trigger');
      const collapse = $(this).find('.accordion-collapse');

      $(trigger).first().addClass('accordion-open');
      $(collapse).first().show();

      $(trigger).each(function(){
        $(this).on('click', function(){
          $(this).siblings('.accordion-collapse').slideToggle();
          $(this).toggleClass('accordion-open');
          $(this).parent().siblings('.accordion-item').find('.accordion-trigger').removeClass('accordion-open');
          $(this).parent().siblings('.accordion-item').find('.accordion-collapse').slideUp();
        });
      });
    }

    $('.accordion').accordion();
});


    // if(lengths >=4){
    //     $( "#MainMenu #accordion > .card .collapse a" ).each(function() {
    //         if($(this).text() == $('.container-content .breadcrumb .container > span > span > span:last-child a').text()) {
    //             $(this).addClass('active');
    //         }
    //     });
    // } 

    // var lengths = $('.breakcrum a').length;
    //     if(lengths >=3){
    //         var Titlemain = $('.breakcrum a:nth-of-type(3)').text();
        
            
    //     } else{
    //         var Titlemain = $('.breakcrum a:last-child').text();
    //     }
    //     $('#main-menu > li > a').each(function(){
    //         if ($(this).text() == Titlemain) {
    //             $(this).removeClass('current');
    //             $(this).addClass('current');    
    //         }

    //     });

    /* var ac = $('.module_titleh4').text();
        $('#MainMenu > .accordion > .card').each(function(){
            if ($(this).children('a').text() == ac) {
                $(this).removeClass('active');
                $(this).addClass('active');
            }
        }); */

</script>
