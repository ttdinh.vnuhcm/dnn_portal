
(function ($) {
    "use strict";

    // Initiate the wowjs
    // wrap in  $( document ).ready(function() to solve undefine wow 
    $(document).ready(function () {
        new WOW().init()
    });

    $('.speaker-carousel').owlCarousel({
        loop: true,
        margin: 24,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    });

    $('.about-carousel').owlCarousel({
        loop: false,
        margin: 24,
        nav: true,
        dots: false,
        navText: ["<span class='material-symbols-outlined'>west</span>", "<span class='material-symbols-outlined'>east</span>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });

    $('.news-carousel').owlCarousel({
        loop: false,
        margin: 24,
        nav: false,
        owl2row: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    });

    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.back-to-top').fadeIn('fast');
        } else {
            $('.back-to-top').fadeOut('fast');
        }
    });
    $('.back-to-top').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 1000, 'easeInOutExpo');
        return false;
    });


})(jQuery);


// Smooth scroll to id
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});


// ava button
var inputs = document.querySelectorAll('.inputAva');
Array.prototype.forEach.call(inputs, function (input) {
    var label = input.nextElementSibling,
        labelVal = label.innerHTML;

    input.addEventListener('change', function (e) {
        var fileName = '';
        if (this.files && this.files.length > 1)
            fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
        else
            fileName = e.target.value.split('\\').pop();

        if (fileName)
            label.querySelector('span').innerHTML = fileName;
        else
            label.innerHTML = labelVal;
    });
});

//checkbox
(function () {
    const form = document.querySelector('#sectionForm');
    const checkboxes = form.querySelectorAll('input[type=checkbox]');
    const checkboxLength = checkboxes.length;
    const firstCheckbox = checkboxLength > 0 ? checkboxes[0] : null;

    function init() {
        if (firstCheckbox) {
            for (let i = 0; i < checkboxLength; i++) {
                checkboxes[i].addEventListener('change', checkValidity);
            }

            checkValidity();
        }
    }

    function isChecked() {
        for (let i = 0; i < checkboxLength; i++) {
            if (checkboxes[i].checked) return true;
        }

        return false;
    }

    function checkValidity() {
        const errorMessage = !isChecked() ? 'At least one checkbox must be selected.' : '';
        firstCheckbox.setCustomValidity(errorMessage);
    }

    init();
})();

/* Add current class to Small-navbar */
    let sections = document.querySelectorAll('section');
    let navLinks = document.querySelectorAll('.navbar-collapse > .navbar-nav > li > a');

    window.onscroll = () =>{
        sections.forEach(sec =>{
            let top = window.scrollY;
            let offset = sec.offsetTop - 100;
            let height = sec.offsetHeight;
            let id = sec.getAttribute('id');

            if (top >= offset && top < offset + height){
                navLinks.forEach(links =>{
                    links.classList.remove('active');
                    document.querySelector('.navbar-collapse > .navbar-nav > li > a[href*=' + id + ']').classList.add('active');
                })
            }
        })
    }



//Toggle in/out

/* $(".plan-details .plan-daygroup").each(function() {
    $(this).children(span).click(function() {
        $(this).find('.plan-info').toggle(100);
    })
}); */

$(".plan-details .plan-daygroup .plan-day").each(function() {
    $(this).on("click", function(){
        $(this).closest('.plan-daygroup').find('.plan-info').slideToggle(500);
    });
});

$(".plan-details .plan-info .has-mini-plan .main-plan").each(function() {
    $(this).on("click", function(){
        $(this).closest('.has-mini-plan').find('.mini-plan').slideToggle(500);
        if ($(this).closest('.has-mini-plan').hasClass("collapsed")) {
            $(this).closest('.has-mini-plan').removeClass("collapsed");
        } else {
            $(this).closest('.has-mini-plan').addClass("collapsed");
        }
    });
});