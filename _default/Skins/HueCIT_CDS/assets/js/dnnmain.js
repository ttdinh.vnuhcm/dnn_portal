﻿$(document).ready(function ($) {
    "use strict";

    // var headerEle = function () {
    //     var $headerHeight = $('.header').height();
    //     $('.hidden-header').css({ 'height': $headerHeight + "px" });
    // };

    // $(window).load(function () {
    //     headerEle();
    // });

    // $(window).resize(function () {
    //     headerEle();
    // });

    // $(function () {
    //     if ($('form').hasClass('showControlBar')) $('.header').addClass('admin-bar');
    // });






    /*----------------------------------------------------*/
    /*	Sticky Header
	/*----------------------------------------------------*/

    (function () {

        var docElem = document.documentElement,
			didScroll = false,
			changeHeaderOn = 100;
        document.querySelector('header');

        function init() {
            window.addEventListener('scroll', function () {
                if (!didScroll) {
                    didScroll = true;
                    setTimeout(scrollPage, 250);
                }
            }, false);
        }

        function scrollPage() {
            var sy = scrollY();
            if (sy >= changeHeaderOn) {
                //$('.top-bar').slideUp(300);
                $("header").addClass("fixed-header");

                $("#rasaWebchatPro").addClass("scroll-chatbot");
                //$('.navbar-brand').css({ 'padding-top': 19 + "px", 'padding-bottom': 19 + "px" });

                if (/iPhone|iPod|BlackBerry/i.test(navigator.userAgent) || $(window).width() < 479) {
                    $('.navbar-default .navbar-nav > li > a').css({ 'padding-top': 0 + "px", 'padding-bottom': 0 + "px" })
                } else {
                    $('.navbar-default .navbar-nav > li > a').css({ 'padding-top': 20 + "px", 'padding-bottom': 20 + "px" })
                    $('.search-side').css({ 'margin-top': -7 + "px" });
                };

            }
            else {
                //$('.top-bar').slideDown(300);
                $("header").removeClass("fixed-header");
                $("#rasaWebchatPro").removeClass("scroll-chatbot");
                //$('.navbar-brand').css({ 'padding-top': 27 + "px", 'padding-bottom': 27 + "px" });

                if (/iPhone|iPod|BlackBerry/i.test(navigator.userAgent) || $(window).width() < 479) {
                    $('.navbar-default .navbar-nav > li > a').css({ 'padding-top': 0 + "px", 'padding-bottom': 0 + "px" })
                } else {
                    $('.navbar-default .navbar-nav > li > a').css({ 'padding-top': 28 + "px", 'padding-bottom': 28 + "px" })
                    $('.search-side').css({ 'margin-top': 0 + "px" });
                };

            }
            didScroll = false;
        }

        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }

        init();



    })();

    // Lazy Load Images using Intersection Observer
    (function () {
        var observer = new IntersectionObserver(onIntersect);

        document.querySelectorAll("[data-lazy]").forEach((img) => {
            observer.observe(img);
        });

        function onIntersect(entries) {
            entries.forEach((entry) => {
                if (entry.target.getAttribute("data-processed") || !entry.isIntersecting)
                    return true;
                entry.target.setAttribute("src", entry.target.getAttribute("data-src"));
                entry.target.setAttribute("data-processed", true);
            });
        }
    })();

    /*----------------------------------------------------*/
    /*	Active Menu
	/*----------------------------------------------------*/

    $( ".sm-contra > li ul li" ).each(function() {
        if($(this).children('a').hasClass('current')) {
            $(this).siblings('a').addClass('current');
            $(this).parents().siblings('a').addClass('current');
        }
    });

    /*----------------------------------------------------*/
    /*	Back Top Link
	/*----------------------------------------------------*/

    var offset = 200;
    var duration = 500;
    $(window).scroll(function () {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(400);
        } else {
            $('.back-to-top').fadeOut(400);
        }
    });
    $('.back-to-top').click(function (event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 600);
        return false;
    })



    
      
      



    





});
document.addEventListener("DOMContentLoaded", function() {
    var lazyloadImages;    
  
    if ("IntersectionObserver" in window) {
      lazyloadImages = document.querySelectorAll(".lazy");
      var imageObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            var image = entry.target;
            image.classList.remove("lazy");
            imageObserver.unobserve(image);
          }
        });
      });
  
      lazyloadImages.forEach(function(image) {
        imageObserver.observe(image);
      });
    } else {  
      var lazyloadThrottleTimeout;
      lazyloadImages = document.querySelectorAll(".lazy");
      
      function lazyload () {
        if(lazyloadThrottleTimeout) {
          clearTimeout(lazyloadThrottleTimeout);
        }    
  
        lazyloadThrottleTimeout = setTimeout(function() {
          var scrollTop = window.pageYOffset;
          lazyloadImages.forEach(function(img) {
              if(img.offsetTop < (window.innerHeight + scrollTop)) {
                img.src = img.dataset.src;
                img.classList.remove('lazy');
              }
          });
          if(lazyloadImages.length == 0) { 
            document.removeEventListener("scroll", lazyload);
            window.removeEventListener("resize", lazyload);
            window.removeEventListener("orientationChange", lazyload);
          }
        }, 20);
      }
  
      document.addEventListener("scroll", lazyload);
      window.addEventListener("resize", lazyload);
      window.addEventListener("orientationChange", lazyload);
    }
  })
  
  