﻿<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<!--#include file="layouts/default/_includes-top-admin.ascx" -->

<script type="text/javascript">
    var body = document.body;
    body.classList.add("sidebar-mini");
    body.classList.add("sidebar-collapse");
</script>


<div class="wrapper">
   <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav top-info">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button">
            <i class="material-symbols-rounded">menu</i>
            <%-- <%= Convert.ToString(PortalSettings.PortalName)%> --%>
            <span><%= Convert.ToString(PortalSettings.ActiveTab.Title)%></span>
        </a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <div class="navbar-nav ml-auto">
      <div class="right-panel d-flex">
          <i class="icon icon-real-estate-agent"></i>
          <div class="info">
          <a href="/" class="d-flex">
              <img src="/Portals/0/Logo_navright.png" alt="" style="margin-right:8px">Website Cổng chuyển đổi số tỉnh Thừa Thiên Huế
          </a>
          </div>
      </div>
      <div class="info-user d-none">
          <div id="UserPane" runat="server"></div>
          <div class="user-action">
              <div class="change-info">
                  <div class="user-info">
                      <ul class="list-inline general-functions">
                          <%  If DotNetNuke.Security.PortalSecurity.IsInRole("Registered Users") Then %>
                              <li class="list-inline-item text-xs-center">
                                  <div class="profile-holder text-xs-center"><dnn:USER ID="dnnUserProfile" runat="server" LegacyMode="false" CssClass="profile" /></div>
                              </li>
                          <% Else %>
                              <!-- Register -->
                              <li class="list-inline-item text-xs-center">
                                  <dnn:USER ID="dnnUserRegister" runat="server" LegacyMode="false" CssClass="register" Text="&lt;i class=&quot;fa fa-user-plus&quot;&gt;&lt;/i&gt;" />
                              </li>
                          <% End If %>
                      </ul>
                  </div>
                  <div class="user-action-expand">
                      <span class="material-symbols-rounded">
                          expand_more
                          </span>
                  </div>
              </div>
              <div class="user-action-box">
                  <div class="update-info">
                      <div class="notification-holder text-xs-center"><dnn:USER ID="dnnUserNotifications" runat="server" LegacyMode="false" CssClass="notifications" /></div>
                  </div>
                  <div class="log-out">
                      <a href="/logoff.aspx">Thoát</a></li>
                  </div>
              </div>
          </div>
      </div>
  </div>
  </nav>
  <!-- /.navbar -->
  <!-- elevation-4 -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary">
    <!-- Brand Logo -->
    <div class="brand-logo">
      <a href="/" class="brand-link">
        <%-- <img src="/Portals/0/<%= Convert.ToString(PortalSettings.LogoFile)%>" alt="<%= Convert.ToString(PortalSettings.PortalName)%>" class="brand-image"> --%>
        <img src="/Portals/0/Logo_admin.png" alt="<%= Convert.ToString(PortalSettings.PortalName)%>" class="brand-image">   
      </a>
      <a class="push" data-widget="pushmenu" href="#" role="button">
            <i class="material-symbols-rounded">menu</i>
        </a>
    </div>


    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <dnn:menu id="NAV1" runat="server" NodeSelector="117" menustyle="navs/default-sidebar" />
      </nav>
      <!-- /.sidebar-menu -->
        <!-- Main Footer -->
      <div class="nav-footer">
        <div class="footer-right">
          Thiết kế và xây dựng: <a href="https://www.huecit.vn" target="_blank">HueCIT</a>
        </div>
      </div>
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header d-none">
        <h6 class="m-0"><%= Convert.ToString(PortalSettings.ActiveTab.Title)%></h6>
        <!-- <i class='material-symbols-rounded'>chevron_right</i> -->
        <div class="breadcrumb">
            <dnn:Breadcrumb runat="server" RootLevel="-1" Separator="<span class='icon-breadcrumb' style='margin: 0 8px;'>/</span>" CssClass="breadcrumb-item" id="dnnBreadcrumb"></dnn:Breadcrumb>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="content-container">
            <div id="ContentPane" runat="server"></div>
            <div class="row">
                <div id="LeftPane" runat="server" class="col-md-3"></div>
                <div id="RightPane" runat="server" class="col-md-9"></div>
            </div>
        </div>

    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!--#include file="layouts/default/_includes-bottom.ascx" -->
<dnn:DnnJsInclude ID="DnnJsMain12" runat="server" FilePath="assets/js/adminlte.min.js"  PathNameAlias="SkinPath"  Name="dnnmain12" Version="1.0.0" />

<script type="text/javascript">
        function changeSmallLogo() {
          $(".sidebar-mini.sidebar-collapse .main-sidebar .brand-logo .brand-image").attr("src","/Portals/0/mini_logo.png");
        }
        function changeLargeLogo() {
          $(".sidebar-mini.sidebar-collapse .main-sidebar .brand-logo .brand-image").attr("src","/Portals/0/Logo_admin.png");
        }
    $(document).ready(function ($) {
        $('.user-action').click(function(){
            $('.user-action-box').toggle(500);
        });

        $('.nav-treeview li .active').each(function(){
             $(this).parents('.nav-treeview').addClass('active-content');
             $(this).parents('li.nav-item').addClass('menu-is-opening menu-open');
             $(this).parents('.nav-treeview').siblings('.nav-link').addClass('active');
        });
        changeSmallLogo();
        $( ".main-sidebar .brand-logo .push" ).on( "click", function() {
          changeLargeLogo();
        });
        $( ".main-header .top-info .nav-link" ).on( "click", function() {
          if ($('.main-header .top-info .nav-link i').css('display') == 'none') {
            changeSmallLogo();
          } else {
            changeLargeLogo();
          }
        });
        $( ".sidebar-collapse .main-sidebar" ).hover(
          function() {
            $(this).addClass("hover");
            changeLargeLogo();
          }, function() {
            $(this).removeClass("hover");
            changeSmallLogo();
            $('.nav-sidebar .nav-item').removeClass('menu-is-opening menu-open');
          }
        );

        $(function () {
          $('.select2-control').each(function () {
            $(this).select2({
              theme: 'bootstrap4',
              width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            });
          });
        });
    });

    $('.main-sidebar .nav-sidebar > .nav-item > .nav-treeview').on('change', function() {
      if ($(this).css('display') == 'block')
        {
          $(this).addClass('menu-is-opening menu-open')
        }
      else {
        $(this).removeClass('menu-is-opening menu-open')
      }
    });


</script>

