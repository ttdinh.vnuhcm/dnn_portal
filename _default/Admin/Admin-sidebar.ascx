<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<!--#include file="layouts/default/_includes-top-admin.ascx" -->

<script type="text/javascript">
    var body = document.body;
    body.classList.add("sidebar-mini");
</script>


<div class="wrapper">
   <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button">
            <i class="material-icons-outlined">menu</i>
            Hệ thống Quản lý cơ sở dữ liệu Quỹ nhà, đất
        </a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <div class="navbar-nav ml-auto">
        <div class="right-panel d-flex">
            <i class="icon icon-real-estate-agent"></i>
            <div class="info">
              <a href="/" class="d-block">Trang thông tin giao dịch BĐS</a>
            </div>
        </div>
        <div class="logout d-flex">
            <i class="icon icon-logout"></i>
            <a href="/logoff.aspx">Thoát</a></li>
        </div>
    </div>
  </nav>
  <!-- /.navbar -->
  <!-- elevation-4 -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="/Portals/0/logo_user.png" alt="Quản trị hệ thống" class="brand-image img-circle">
      <span class="brand-text font-weight-light">Quản trị hệ thống</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <dnn:menu id="NAV1" runat="server" NodeSelector="35" menustyle="navs/default-sidebar" />
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <%--<h6 class="m-0"><%= Convert.ToString(PortalSettings.ActiveTab.Title)%></h6>--%>
        <!-- <i class='material-icons-outlined'>chevron_right</i> -->
        <div class="breadcrumb">
            <dnn:Breadcrumb runat="server" RootLevel="-1" Separator="<span class='icon-breadcrumb' style='margin: 0 8px;'>/</span>" CssClass="breadcrumb-item" id="dnnBreadcrumb"></dnn:Breadcrumb>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="content-container">
            <div id="ContentPane" runat="server"></div>
            <div id="ContentPane" class="d-none">
                <div class="DnnModule DnnModule-DNNTinBai DnnModule-382"><a name="382"></a>

                    <div class="contraC main">
                        <div id="dnn_ctr_ContentPane" class="contentpane"><!-- Start_Module_382 -->
                            <div id="dnn_ctr382_ModuleContent" class="DNNModuleContent ModTENMODULE">

                                <div class="form-container form-admin form-tab">
                                    <!--class="form-container" Mặc định sử dụng cho các form -->
                                    <!--class="form-tab" sử dụng cho form có dùng tabs -->
                                    <!--class="form-admin" sử dụng cho form dùng trong Quản trị -->
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="id-tab-1" data-toggle="tab" href="#id-tabpane-1" role="tab" aria-controls="id-tabpane-1" aria-selected="true">id-tabpane-1</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="id-tab-2" data-toggle="tab" href="#id-tabpane-2" role="tab" aria-controls="id-tabpane-2" aria-selected="false">id-tabpane-2</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="id-tab-3" data-toggle="tab" href="#id-tabpane-3" role="tab" aria-controls="id-tabpane-3" aria-selected="false">id-tabpane-3</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="id-tabpane-1" role="tabpanel" aria-labelledby="id-tab-1">
                                            <div class="form-general">
                                                <!-- Trường hợp không có tab thì sử dụng html bắt đầu từ đây Ngoài vẫn bọc 1 div class="form-container" -->
                                                <h6 class="title-form">Thông tin</h6>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="inputEmail4">Email</label>
                                                        <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputPassword4">Password</label>
                                                        <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputAddress">Address</label>
                                                    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1">Example textarea</label>
                                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="inputAddress2">Address 2</label>
                                                        <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputCity">City</label>
                                                        <input type="text" class="form-control" id="inputCity">
                                                    </div>
                                                </div>
                                                <h6 class="title-form">Select 2</h6>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label>Select 2</label>
                                                        <select class="select2-control" data-placeholder="Choose one thing" data-allow-clear="1">
                                                            <option></option>
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="inputState">State</label>
                                                        <select id="inputState" class="form-control">
                                                            <option selected>Choose...</option>
                                                            <option>...</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label for="inputZip">Zip</label>
                                                        <input type="text" class="form-control" id="inputZip">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" id="gridCheck">
                                                        <label class="form-check-label" for="gridCheck">
                                                            Check me out
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                        <label class="form-check-label" for="inlineRadio1">1</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                        <label class="form-check-label" for="inlineRadio2">2</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" disabled>
                                                        <label class="form-check-label" for="inlineRadio3">3 (disabled)</label>
                                                    </div>
                                                </div>
                                                <div class="form-group text-right">
                                                    <button type="button" class="btn btn-primary">Primary</button>
                                                    <button type="button" class="btn btn-secondary">Secondary</button>
                                                    <button type="button" class="btn btn-success">Success</button>
                                                    <button type="button" class="btn btn-danger">Danger</button>
                                                    <button type="button" class="btn btn-warning">Warning</button>
                                                    <button type="button" class="btn btn-info">Info</button>
                                                    <button type="button" class="btn btn-light">Light</button>
                                                    <button type="button" class="btn btn-dark">Dark</button>
                                                    <button type="button" class="btn btn-link">Link</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="id-tabpane-2" role="tabpanel" aria-labelledby="id-tab-2">...</div>
                                        <div class="tab-pane fade" id="id-tabpane-3" role="tabpanel" aria-labelledby="id-tab-3">...</div>
                                    </div>
                                </div>

                            </div><!-- End_Module_382 -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- Default to the left -->
    <div class="footer-left">
        &copy; 2022 <strong>Trung tâm Phát triển Quỹ đất tỉnh Thừa Thiên Huế</strong>
    </div>
    <!-- To the right -->
    <div class="footer-right">
      Thiết kế và xây dựng: <strong>HueCIT</strong>
    </div>
  </footer>
</div>



<!--#include file="layouts/default/_includes-bottom.ascx" -->
<dnn:DnnJsInclude ID="DnnJsMain12" runat="server" FilePath="assets/js/adminlte.min.js"  PathNameAlias="SkinPath"  Name="dnnmain12" Version="1.0.0" />

<script type="text/javascript">
    $(document).ready(function ($) {

        $('.nav-treeview li .active').each(function(){
             $(this).parents('.nav-treeview').addClass('active-content');
             $(this).parents('li.nav-item').addClass('menu-is-opening menu-open');
        });

        $(function () {
          $('.select2-control').each(function () {
            $(this).select2({
              theme: 'bootstrap4',
              width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            });
          });
        });

    });


</script>

