﻿<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="assets/js/tether.min.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Name="trehter" Version="1.4.0" />

<dnn:DnnJsInclude ID="DnnJsBootStrap" runat="server" FilePath="assets/js/bootstrap.bundle.min.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Name="boostrap" Version="4.5.2" />
<dnn:DnnJsInclude ID="DnnJsSmartMenus" runat="server" FilePath="assets/js/jquery.smartmenus.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Name="smartmenus" Version="1.0.1" />
<dnn:DnnJsInclude ID="DnnJssticky" runat="server" FilePath="assets/js/jquery.sticky.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Name="sticky" Version="1.0.1" />
<%--<dnn:DnnJsInclude ID="DnnJsOwlcarousel" runat="server" FilePath="assets/js/owl.carousel.min.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Name="Owlcarousel" Version="2.3.4" />--%>
<dnn:DnnJsInclude ID="DnnJsUtils" runat="server" FilePath="assets/js/utils.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Name="utils" Version="1.0.0" />
<dnn:DnnJsInclude ID="DnnJsMain" runat="server" FilePath="assets/js/dnnmain.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Name="dnnmain" Version="1.0.0" />

<dnn:DnnJsInclude ID="DnnJSDatatable" runat="server" FilePath="dist/DataTables/datatables.min.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Name="Datatable" Version="1.11.3" />

<dnn:DnnJsInclude ID="DnnJSselect2" runat="server" FilePath="dist/Select2/select2.min.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Name="select2" Version="1.11.3" />
