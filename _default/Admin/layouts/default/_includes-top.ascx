﻿
<dnn:META ID="mobileScale" runat="server" Name="viewport" Content="width=device-width, initial-scale=1.0" />
<dnn:JQUERY ID="dnnjQuery" runat="server" jQueryHoverIntent="true" />
<dnn:DnnCssInclude ID="DnnCssBootStrap" runat="server" FilePath="assets/css/bootstrap.css" PathNameAlias="SkinPath" Name="bootstrap" Version="4.0.1-alpha.6" />
<dnn:DnnCssInclude ID="DnnCssThemify" runat="server" FilePath="assets/css/themify-icons.css" PathNameAlias="SkinPath" Name="Themify" Version="1.0.0" />
<dnn:DnnCssInclude ID="DnnCssMaterial" runat="server" FilePath="assets/css/material-icons-full.css" PathNameAlias="SkinPath" Name="Material" Version="1.0.0" />
<dnn:DnnCssInclude ID="DnnCssSmartMenus" runat="server" FilePath="assets/css/smartmenus.css" PathNameAlias="SkinPath" Name="smartmenus" Version="1.0.1" />
<dnn:DnnCssInclude ID="DnnCssOwlcarousel" runat="server" FilePath="assets/css/owl.carousel.min.css" PathNameAlias="SkinPath" Name="Owlcarousel" Version="2.3.4" />
<dnn:DnnCssInclude ID="DnnCssOwltheme" runat="server" FilePath="assets/css/owl.theme.default.min.css" PathNameAlias="SkinPath" Name="Owltheme" Version="2.3.4" />
<dnn:DnnCssInclude ID="DnnCssAnimate" runat="server" FilePath="assets/css/animate.min.css" PathNameAlias="SkinPath" Name="animatecss" Version="3.5.2" />
<dnn:DnnCssInclude ID="DnnCssStyle" runat="server" FilePath="assets/css/dnnstyle.css" PathNameAlias="SkinPath" Name="dnnstyle" Version="1.0.1" />

<dnn:DnnCssInclude ID="DnnCssModule" runat="server" FilePath="assets/css/dnnstyle-update.css?v=0.0.1.css" PathNameAlias="SkinPath" Name="dnnmodule" Version="0.0.1" />
<dnn:DnnCssInclude ID="DnnCssResponsive" runat="server" FilePath="assets/css/responsive.css?v=0.0.1" PathNameAlias="SkinPath" Name="dnnResponsive" Version="1.0.1" />
<dnn:DnnCssInclude ID="DnnCssSwiper" runat="server" FilePath="assets/css/swiper-bundle.min.css" PathNameAlias="SkinPath" Name="dnnSwiper" Version="8.3.2" />


<!-- Theme pre-defined color scheme -->
<dnn:DnnCssInclude ID="DnnCssColorScheme" runat="server" FilePath="assets/css/colors/_base.css?v=0.0.1" PathNameAlias="SkinPath" Name="colorscheme" Version="1.0.1" />
<!-- Theme pre-defined color scheme -->

<dnn:DnnJsInclude ID="DnnJsSwiper" runat="server" FilePath="assets/js/swiper-bundle.min.js" PathNameAlias="SkinPath" Name="Swiper" Version="8.3.2" />
<dnn:DnnJsInclude ID="DnnJsOwlcarousel" runat="server" FilePath="assets/js/owl.carousel.min.js" PathNameAlias="SkinPath" Name="Owlcarousel" Version="2.3.4" />

